@author Adam Lastowka

Cubemapconverter is a short Processing sketch designed to dissect single cubemap images into six individual textures.
Usage is pretty simple. Put the images you want to convert into the "data" folder (any image format is fine), and run the .pde.
The "cubemaps" folder (the program will create it) will now contain folders for each image in the "data" folder, each one containing six .bmp images.
The .bmp images will be flipped, this is intentional and is done here because it's easier to flip them in processing than it is in C++ (OpenGL is finnicky and likes the texture data to be upside-down when supplied).

Important note: cubemaps should be supplied in an image where the unfolded cubemap is in a 't' shape rotated 90 degrees counter-clockwise.