import java.io.File;

PImage img;
int s;
int w;
void setup() {
  smooth(8);
  size(100, 100, P2D);
  
  //Image flipping
  scale(1, -1);
  translate(0, -s);
  
  String[] fnames = listFiles(dataPath(""));
  
  for(String name : fnames) {
    println("Converting " + name + "...");
    img = loadImage(name);
    w = img.width/4;
    s = w;
    PGraphics pg = createGraphics(w, w, P2D);
    pg.smooth(8);
    pg.beginDraw();
    
    pg.scale(1, -1);
    pg.translate(0, -s);
    
    String sname = name.split("\\.")[0];
    
    //image, screen coordinates(xywh), image coordinates to grab from (x1 y1 x2 y2)
    pg.image(img, 0, 0, s, s, w*2, w, w*3, w*2);
    pg.save("cubemaps/" + sname + "/0.bmp");
    
    pg.image(img, 0, 0, s, s, 0, w, w, w*2);
    pg.save("cubemaps/" + sname + "/1.bmp");
    
    pg.image(img, 0, 0, s, s, w, 0, w*2, w);
    pg.save("cubemaps/" + sname + "/2.bmp");
    
    pg.image(img, 0, 0, s, s, w, w*2, w*2, w*3);
    pg.save("cubemaps/" + sname + "/3.bmp");
    
    pg.image(img, 0, 0, s, s, w, w, w*2, w*2);
    pg.save("cubemaps/" + sname + "/4.bmp");
    
    pg.image(img, 0, 0, s, s, w*3, w, w*4, w*2);
    pg.save("cubemaps/" + sname + "/5.bmp");
    
    pg.endDraw();
  }
  println("Done!");
}
void draw() {
  background(0, 255, 0);
}

public String[] listFiles(String folderPath) {
  final File folder = new File(folderPath);
  println(folder.getAbsolutePath());
  final File[] items = folder.listFiles();
  String[] fnames = new String[items.length];
  int i = 0;
  for (final File f : items) {
    if(!f.isDirectory()) {
      fnames[i] = f.getName();
      i++;
    }
  }
  String[] names2 = new String[i];
  for(int j = 0; j < names2.length; j++) {
    names2[j] = fnames[j];
  }
  return names2;
}
