@Author Benjamin Welsh

io_scene_alm folder goes in C:\Program Files\Blender Foundation\Blender\2.##\scripts\addons (## is version number)
To enable in blender, go to File -> User Preferences -> Add-ons, search for alm, and check the little box. Click "save user settings" at the bottom to load it by default.

The ALM_npp_lang.xml is a language formatting for notepad++. To use, go to Language -> Define Your Language... -> Import, and import the file. to select the language, go to Language, and it should appear near the bottom.
*Note: the language is configured for a dark theme (black background, specifically)