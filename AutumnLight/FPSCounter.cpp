//@Authors Adam Lastowka & Benjamin Welsh

//FPSCounter keeps track of the frame count, time between frames, and a number of averaged stats.
//It is called every frame by FPSCounter.tick()
//Per frame, it tracks: 
//framesPassed ==> total frames, incremented by 1 per call of tick()
//deltaTime ==> the time since the last call of tick()
//Every averaging period (1 second), it calculates:
//msPerFrame ==> avg ms/frame in period
//FPS ==> frames in period/time of period
//msDev ==> the average of the deviations==>(frameMS-pastPeriodAvgMS)

#include "FPSCounter.h"
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <sstream>

void FPSCounter::tick() {
	double timeGet = glfwGetTime();
	deltaTime = timeGet - lastTime; //Current frame ms
	lastTime = timeGet;
	msDevSum += abs(deltaTime*1000.0 - msPerFrame);

	nbFrames++; //frames from last update
	framesPassed++; //total
	double avgTime = timeGet - lastAvgTime;
	if (avgTime >= 1.0){
		//std::cout << 1000.0 / double(nbFrames) << " ms/Frame " << double(nbFrames) << " FPS" << std::endl;
		msPerFrame = (avgTime / nbFrames)*1000.0;
		FPS = nbFrames / avgTime;
		msDev = msDevSum / nbFrames;
		nbFrames = 0;
		msDevSum = 0.0;
		lastAvgTime += 1.0;
	}
}

FPSCounter::FPSCounter() {
	fprintf(stdout, "FPSCounterMKII Initalized!\n\n");
}