#include <iostream>
#include <stdio.h>
#include <string.h>

#include <fstream>
#include <ostream>
#include "teeS.h"
#include "logTools.h"

void makeLogTee(std::string name = "log.txt"){
	//std::ostream console(std::cout.rdbuf()); //make a ostream to hold the console stream
	(*qconsole).rdbuf(std::cout.rdbuf()); // assign the console stream to var console
	(*qlogf).open(name.c_str());  // The log file stream std::ofstream 
	teestream qteeLog (*qlogf, *qconsole); //make a tee to the console and the logfile
	}

void configOutputs(){
	if (qERR_TO_CON)
		std::cerr.rdbuf((*qteeLog).rdbuf()); //redirect cerr to the tee
	else
		std::cerr.rdbuf((*qlogf).rdbuf());
}
//	if (qLOG_TO_CON)
//		std::clog.rdbuf((qteeLog).rdbuf()); //redirect clog to the tee
//	else
//		std::clog.rdbuf((logf).rdbuf());
//
//	if (qOUT_TO_CON)
//		std::cout.rdbuf((qteeLog).rdbuf()); //redirect cout to the tee
//	else
//		std::cout.rdbuf((qlogf).rdbuf());
//}