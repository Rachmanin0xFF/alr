#version 330 core
#extension GL_NV_shadow_samplers_cube : enable

in vec3 s_pos;
in vec3 normal_in;
in vec2 texCoord;
in vec3 tangent;
in vec3 bitangent;

uniform sampler2D TA; // Albedo texture
uniform sampler2D TB; // Ambient Lightmap
uniform sampler2D TC; // Normal map
uniform samplerCube TD; // Environment map

uniform float do_smooth_shading;

//Texture boolean uniforms
uniform float albedo_texture_on;
uniform float ambient_texture_on;
uniform float normal_texture_on;

//Non-shiny lighting uniforms
uniform float ambient_intensity;
uniform vec3 diffuse_color;
uniform float diffuse_intensity;

//Shiny lighting uniforms
uniform float do_reflections;
uniform vec3 reflect_color;
uniform float reflect_intensity;
uniform float specular_intensity;
uniform float specular_color;
uniform float hardness;

//haha crysis 3 i stole ur normal buffer encoding kind of (pls don't sue thx)
vec2 encodeNormal(vec3 n) {
    float f = sqrt(8*n.z + 8.0);
    return n.xy/f + 0.5;
}

void main(void) {
	//Smooth/flat shading
	//Fetch normal regularly, if we're not smooth shading, calculate normal manually using position data.
	vec3 normal = normal_in;
	if(do_smooth_shading < 0.5) {
		//This process is actually pretty speedy.
		normal = normalize(cross(dFdx(s_pos), dFdy(s_pos)));
	}
	
	//Normal mapping
	if(normal_texture_on > 0.5) {
		vec2 tn = texture2D(TC, texCoord).xy;
		tn.y = 1.0 - tn.y;
		if(tn != vec2(0.0, 0.0)) {
			vec3 N0 = normalize(normal);
			vec3 T0 = normalize(tangent);
			vec3 B0 = normalize(bitangent);
			
			vec3 nmap;
			nmap.xy = ((2.0*tn) - 1.0)*0.5;
			nmap.xy = nmap.xy;
			nmap.z = sqrt(1.0 - dot(nmap.xy, nmap.xy));
			N0 = normalize((T0*nmap.x) + (B0*nmap.y) + (N0*nmap.z));
			normal = N0;
		}
	}
	
	//Write [0]
	vec3 albedo = diffuse_color;
	if(albedo_texture_on > 0.5) {
		albedo *= texture2D(TA, texCoord).rgb*diffuse_color;
	}
	gl_FragData[0].rgb = albedo;
	gl_FragData[0].a = diffuse_intensity;
	
	//Write [1]
	gl_FragData[1].rg = encodeNormal(normal);
	gl_FragData[1].b = specular_intensity;
	gl_FragData[1].rgb = albedo*dot(normal, vec3(0, 0, 1));
	//Compress hardness for accuracy (we're working in RGBA8 precision most likely)
	gl_FragData[1].a = 1.0/(hardness*0.03125 + 1.0); //0.03125 because 1.0*0.03125 = 1.0/32.0
	
	//Write [2]
	//THIS WOULD BE GOOD TO ELIMINATE ALTOGETHER ONCE DEFERRED PIPELINE IS SET UP
	//Possibly write ambient value in RT0.a, process in deferred shader. This would eliminate the additional write here.
	//Gosh that solution is so sexy.
	if(ambient_texture_on > 0.5) {
		gl_FragData[2].rgb = texture2D(TB, texCoord).rgb*ambient_intensity*diffuse_intensity*albedo;
		gl_FragData[2].a = 1.0;
	} else {
		gl_FragData[2].rgb = albedo*diffuse_intensity;//*ambient_intensity;
	}
}