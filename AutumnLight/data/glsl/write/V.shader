#version 330 core

attribute vec3 uStream0; //Positions
attribute vec3 uStream2; //Normals
attribute vec3 uStream8; //Texture coordinates
attribute vec3 uStream9; //Tangents
attribute vec3 uStream10; //Bitangents

uniform float do_smooth_shading;
uniform float normal_texture_on;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform mat4 modelNormalMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;

out vec3 s_pos;
out vec3 normal_in;
out vec2 texCoord;
out vec3 tangent;
out vec3 bitangent;

void main() {
	gl_Position = modelViewProjectionMatrix*vec4(uStream0, 1.0);
	
	//Normals
	//No speed decrease here, it's a mat3 multiplication either way
	if(do_smooth_shading > 0.5) {
		//Calculate normals regularly
		normal_in = normalize((normalMatrix*vec4(uStream2, 1.0)).xyz);
	} else {
		//Send position data to be processed in fragment shader
		s_pos = (modelViewMatrix*vec4(uStream0, 1.0)).xyz;
	}
	
	//Texture coordinates and texture tangents/bitangents for normal mapping
	texCoord = uStream8.st;
	if(normal_texture_on > 0.5) {
		tangent = (normalMatrix * vec4(uStream9.xyz, 1.0)).xyz;
		bitangent = (normalMatrix * vec4(uStream10.xyz, 1.0)).xyz;
	}
}