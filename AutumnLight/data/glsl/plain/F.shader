#version 330 core
uniform sampler2D TA; // Color
in vec2 texCoord;
out vec4 fragColor;

void main() {
	fragColor = vec4(texture2D(TA, texCoord*1.008-.004).r,texture2D(TA, texCoord).g,texture2D(TA,.004+ texCoord*.992).b,texture2D(TA, texCoord).a);
}