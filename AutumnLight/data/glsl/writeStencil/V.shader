#version 330 core

attribute vec3 uStream0; //Positions
uniform mat4 modelViewProjectionMatrix;

void main() {
	gl_Position = modelViewProjectionMatrix*vec4(uStream0, 1.0);
}