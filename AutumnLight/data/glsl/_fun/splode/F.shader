#version 330 core

uniform sampler2D TA; // Albedo
in vec3 normal;
in vec2 texCoord;

uniform float albedo_texture_on;
uniform vec3 diffuse_color;
uniform float diffuse_intensity;

void main(void) {
	if(albedo_texture_on > 0.5) {
		gl_FragData[0].rgb = diffuse_color*diffuse_intensity*texture2D(TA, texCoord).rgb;
	} else {
		gl_FragData[0].rgb = diffuse_color*diffuse_intensity;
	}
	gl_FragData[0].rgb *= clamp(max(0.0, normal.y) + 0.4, 0.0, 1.0)*1.5;
	gl_FragData[0].a = 1.0;
}