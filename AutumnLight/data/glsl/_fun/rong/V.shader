#version 330 core

attribute vec3 uStream0; //Positions
attribute vec3 uStream2; //Normals
attribute vec3 uStream8; //Texture coordinates
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;
uniform mat3 modelNormalMatrix;
uniform mat4 modelMatrix;
out vec3 normal;
out vec2 texCoord;
out vec3 position;

void main() {
	gl_Position = modelViewProjectionMatrix*vec4(uStream0, 1.0);
	normal = modelNormalMatrix*uStream2;
	texCoord = uStream8.st;
	position = uStream0;
}