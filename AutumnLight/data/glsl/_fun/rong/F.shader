//IMPORTANT NOTICE:
//Rong is out-of-date now, it can't do flat shading.
//All models (or at least indexed vbo models) will be shaded smoothly now when this shader is being used.

#version 330 core
#extension GL_NV_shadow_samplers_cube : enable

uniform sampler2D TA; // Albedo
uniform samplerCube TB;
in vec3 normal;
in vec2 texCoord;
in vec3 position;

uniform float albedo_texture_on;
uniform vec3 diffuse_color;
uniform float diffuse_intensity;
uniform vec3 reflect_color;
uniform float reflect_intensity;
uniform vec3 eyePos;

//Uniforms below here not implemented

uniform float normal_texture_on;
uniform float ambient_intensity;
uniform float specular_intensity;
uniform float specular_color;
uniform float hardness;

//Functions
vec3 diffuse_shader();
vec3 reflect_shader();

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 rand3(float x){
  return 2.0*vec3(rand(gl_FragCoord.xy + vec2(x, x))-0.5, rand(gl_FragCoord.xy + vec2(x*2.3, x*1.7))-0.5, rand(gl_FragCoord.xy + vec2(x*3.1, x*3.3))-0.5);
}

void main(void) {
	
	
	gl_FragData[0].rgb = diffuse_shader()+reflect_shader();
	
	gl_FragData[0].a = 1.0;
}

vec3 diffuse_shader(){
	vec3 ret;
	if(albedo_texture_on > 0.5) {
		ret = diffuse_color*diffuse_intensity*texture2D(TA, texCoord).rgb;
	} else {
		ret = diffuse_color*diffuse_intensity;
	}
	ret *= clamp(max(0.0, normal.y) + 0.4, 0.0, 1.0)*1.5;
	return ret;
}

vec3 reflect_shader(){
	vec3 etp = normalize(eyePos - position);
	float nb = max(0.0, dot(etp, normal));
	vec3 rfr = normalize(reflect(-etp, normal));
	return textureCube(TB, rfr).rgb*0.5;
}
	