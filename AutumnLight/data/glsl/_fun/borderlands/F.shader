#version 330 core
uniform sampler2D TA; // Color
uniform sampler2D TB; // Depth
uniform vec2 RES;
in vec2 texCoord;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
	float s = 0.0;
	float k = 2.0/1920.0;
	s += texture2D(TB, texCoord + vec2(0.0, k*RES.x/RES.y)).r;
	s += texture2D(TB, texCoord + vec2(0.0, -k*RES.x/RES.y)).r;
	s += texture2D(TB, texCoord + vec2(k, 0.0)).r;
	s += texture2D(TB, texCoord + vec2(-k, 0.0)).r;
	s /= 4.0;
	gl_FragData[0] = texture2D(TA, texCoord);
	if(s - texture2D(TB, texCoord).r > 0.000015)
		gl_FragData[0].rgb *= 0.0;
}