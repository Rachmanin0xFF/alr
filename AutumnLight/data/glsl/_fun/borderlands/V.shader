#version 330 core

attribute vec3 uStream0;
out vec2 texCoord;

void main() {
	gl_Position = vec4(uStream0, 1.0);
	texCoord = uStream0.st/2.0 + vec2(0.5, 0.5);
}