#version 330 core
uniform sampler2D TA; // Color
in vec2 texCoord;
out vec4 fragColor;
uniform vec2 RES;

void main() {
	vec3 s0 = texture2D(TA, texCoord).rgb;
	vec3 s1 = texture2D(TA, texCoord + vec2(1.0/RES.x, 0.0)).rgb;
	vec3 s2 = texture2D(TA, texCoord + vec2(-1.0/RES.x, 0.0)).rgb;
	vec3 s3 = texture2D(TA, texCoord + vec2(0.0, 1.0/RES.y)).rgb;
	vec3 s4 = texture2D(TA, texCoord + vec2(0.0, -1.0/RES.y)).rgb;
	fragColor.rgb = 300.0*(s0 - (s1 + s2 + s3 + s4)*0.25);
}