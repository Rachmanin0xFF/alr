//@author Adam Lastowka

//GLUtil is the biggest file in this program. It's a large set of functions designed to carry out OpenGL operations that might require a lot of work to execute otherwise.
//The two main set of functions so far are the uSetUniforms and the uNewFBO_ functions. See descriptions of both below.

#define _CRT_SECURE_NO_WARNINGS

#include "GLUtil.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/GL.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstdarg>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>

#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

//These functions are used for setting uniform variables to shaders (things like resolution, material properties, etc. per-object stuff)

void uSetUniformVertexAttrib(int program, const char* name, int x) {
	int loc = glGetAttribLocation(program, name);
	glLinkProgram(program);
}

void uSetUniform1i(int program, const char* name, int x) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform1i(loc, x);
}

void uSetUniform1f(int program, const char* name, float x) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform1f(loc, x);
}

void uSetUniform2f(int program, const char* name, float x, float y) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform2f(loc, x, y);
}

void uSetUniform2f(int program, const char* name, glm::vec2 v) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform2f(loc, v.x, v.y);
}

void uSetUniform3f(int program, const char* name, float x, float y, float z) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform3f(loc, x, y, z);
}

void uSetUniform3f(int program, const char* name, glm::vec3 v) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniform3f(loc, v.x, v.y, v.z);
}

void uSetUniform3fNoSwitch(const char* name, float x, float y, float z) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform3f(loc, x, y, z);
}

void uSetUniform3fNoSwitch(const char* name, glm::vec3 v) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform3f(loc, v.x, v.y, v.z);
}

void uSetUniformMatrix3x3(int program, const char* name, glm::mat3 x) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniformMatrix3fv(loc, 1, GL_FALSE, &x[0][0]);
}

void uSetUniformMatrix4x4(int program, const char* name, glm::mat4 x) {
	glUseProgram(program);
	int loc = glGetUniformLocation(program, name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, &x[0][0]);
}

void uSetUniform1iNoSwitch(const char* name, int x) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform1i(loc, x);
}

void uSetUniform1fNoSwitch(const char* name, float x) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform1f(loc, x);
}

void uSetUniform2fNoSwitch(const char* name, glm::vec2 v) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform2f(loc, v.x, v.y);
}

void uSetUniform2fNoSwitch(const char* name, float x, float y) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniform2f(loc, x, y);
}

void uSetUniformMatrix3x3NoSwitch(const char* name, glm::mat3 x) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniformMatrix3fv(loc, 1, GL_FALSE, &x[0][0]);
}

void uSetUniformMatrix4x4NoSwitch(const char* name, glm::mat4 x) {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	int loc = glGetUniformLocation(prgram, name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, &x[0][0]);
}

void uPrintMatrix(glm::mat4 m) {
	for (int x = 0; x < 4; x++) {
		std::clog << std::endl;
		for (int y = 0; y < 4; y++)
			std::clog << m[x][y] << " ";
	}
	std::clog << std::endl;
}

void uPrintMatrix(glm::mat4 m, std::ostream &stream) {
	for (int x = 0; x < 4; x++) {
		stream << std::endl;
		for (int y = 0; y < 4; y++)
			stream << m[x][y] << " ";
	}
	stream << std::endl;
}

void uNewGBuffer(GLuint out[], unsigned const int &xres, unsigned const int &yres, const bool &HDR, const bool &HDNorm) {
	GLuint fbo;
	GLuint ds;
	GLuint diffuse;
	GLuint normspec;
	GLuint light;

	glGenFramebuffers(1, &fbo);
	glGenTextures(1, &ds);
	glGenTextures(1, &diffuse);
	glGenTextures(1, &normspec);
	glGenTextures(1, &light);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glBindTexture(GL_TEXTURE_2D, ds);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, xres, yres, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //Switch to linear if no SSAO?
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, ds, 0);

	glBindTexture(GL_TEXTURE_2D, diffuse);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, diffuse, 0);

	glBindTexture(GL_TEXTURE_2D, normspec);
	if (HDNorm) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		std::cout << "High-resolution normal G-Buffer being created..." << std::endl;
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normspec, 0);

	glBindTexture(GL_TEXTURE_2D, light);
	if (HDR) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		std::cout << "HDR-Enabled G-Buffer being created..." << std::endl;
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, light, 0);

	GLenum DrawBuffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, DrawBuffers);

	out[0] = fbo;
	out[1] = ds;
	out[2] = diffuse;
	out[3] = normspec;
	out[4] = light;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cerr << "Error! Framebuffer not properly created!\n\n" << std::endl;
	else
		std::clog << "G-Buffer created successfully." << std::endl;
}

//These functions are used for creating new FBOs.
//Rundown of the fucntion naming process-
//	uNewFBO_ is always the start
//	The remaning letters are ordered in the order of the array the function returns (out[])
//	'F' is the ID of the actual FBO (not any of the textures inside it)
//  'C' stands for color, a standard GL_RGBA8 color texture
//	'D' stands for depth, usually a GL_DEPTH_COMPONENT_16 depth texture
//	'DS' stands for depth-stencil, a depth buffer with a stencil buffer attachment.
//	An 'H' before a C or an D indicates that it is higher-quality (usually 16F for color and 24 for depth)
//	A 'r' after a C or an D indicates that the object is a texture (readable in a shader)
//	
//So, as an example, uNewFBO_FHCrDSr would be uNewFBO_F HCr DSr
//Which would mean the returning array would be:
//[0] - F - Framebuffer ID
//[1] - HCr - A GL_RGBA16F color texture attachment
//[2] - DSr - A readable depth stencil texture attachment

void uNewFBO_FCr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachment GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FHCr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, xres, yres, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachment GL_R16F (readable)\n" << std::endl;
}

void uNewFBO_FDr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glDrawBuffer(GL_NONE);

	out[0] = fbuffer;
	out[2] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachment GL_DEPTH_COMPONENT16 (readable)\n" << std::endl;
}

void uNewFBO_FCrD(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenRenderbuffers(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, xres, yres);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT (renderbuffer) and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FHCrD(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenRenderbuffers(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, xres, yres, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, xres, yres);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT (renderbuffer) and GL_R16F (readable)\n" << std::endl;
}

void uNewFBO_FHCrDr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, xres, yres, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT16 (readable) and GL_R16F (readable)\n" << std::endl;
}

void uNewFBO_FCrDr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT16 (readable) and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrDSr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, xres, yres, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH24_STENCIL8 (readable) and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrCrDr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;
	GLuint normals;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);
	glGenTextures(1, &normals);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, normals);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	GLenum DrawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = normals;
	out[3] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT16 (readable), GL_RGBA8 (readable), and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrCrDSr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;
	GLuint normals;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);
	glGenTextures(1, &normals);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, normals);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, xres, yres, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	GLenum DrawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = normals;
	out[3] = depth;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH24_STENCIL8 (readable), GL_RGBA8 (readable), and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrCrDrCr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;
	GLuint normals;
	GLuint light;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);
	glGenTextures(1, &normals);
	glGenTextures(1, &light);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, normals);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, xres, yres, 0, GL_RG, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glBindTexture(GL_TEXTURE_2D, light);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, light, 0);

	GLenum DrawBuffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT4 };
	glDrawBuffers(3, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = normals;
	out[3] = depth;
	out[4] = light;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT16 (readable), GL_RGBA8 (readable), GL_RGBA8 (readable), and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrCrDrCrCr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;
	GLuint normals;
	GLuint light;
	GLuint data;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);
	glGenTextures(1, &normals);
	glGenTextures(1, &light);
	glGenTextures(1, &data);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, normals);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, xres, yres, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, xres, yres, 0, GL_DEPTH_COMPONENT, GL_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glBindTexture(GL_TEXTURE_2D, light);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, xres, yres, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, light, 0);

	glBindTexture(GL_TEXTURE_2D, data);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, data, 0);

	GLenum DrawBuffers[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5 };
	glDrawBuffers(4, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = normals;
	out[3] = depth;
	out[4] = light;
	out[5] = data;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH_COMPONENT16 (readable), GL_RGBA8 (readable), GL_RGBA8 (readable), and GL_RGBA8 (readable)\n" << std::endl;
}

void uNewFBO_FCrCrDSrCr(GLuint out[], unsigned const int &xres, unsigned const int &yres) {
	GLuint fbuffer;
	GLuint color;
	GLuint depth;
	GLuint normals;
	GLuint light;

	glGenFramebuffers(1, &fbuffer);
	glGenTextures(1, &color);
	glGenTextures(1, &depth);
	glGenTextures(1, &normals);
	glGenTextures(1, &light);

	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);

	glBindTexture(GL_TEXTURE_2D, color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);

	glBindTexture(GL_TEXTURE_2D, normals);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, xres, yres, 0, GL_RG, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals, 0);

	glBindTexture(GL_TEXTURE_2D, depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, xres, yres, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glBindTexture(GL_TEXTURE_2D, light);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xres, yres, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, light, 0);

	GLenum DrawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, DrawBuffers);

	out[0] = fbuffer;
	out[1] = color;
	out[2] = normals;
	out[3] = depth;
	out[4] = light;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		fprintf(stderr, "Error! Framebuffer not properly created!\n\n");
	else
		std::clog << "Framebuffer created successfully with dimensions " << xres << "x" << yres << " and attachments GL_DEPTH24_STENCIL8 (readable), GL_RGBA8 (readable), GL_RGBA8 (readable), and GL_RGBA8 (readable)\n" << std::endl;
}

void uRenderToScreen() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void uRectOnScreenFFP() {
	//DON'T USE THIS IN YOUR CODE THIS IS NOT GOOD//
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(-1.0f, -1.0f);
	glTexCoord2f(0, 1.0f);
	glVertex2f(-1.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);
	glTexCoord2f(1.0f, 0);
	glVertex2f(1.0f, -1.0f);
	glEnd();
}

void uRectOnScreenFFPWriteDepth() {
	//DON'T USE THIS IN YOUR CODE THIS IS NOT GOOD//
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(-1.0f, -1.0f);
	glTexCoord2f(0, 1.0f);
	glVertex2f(-1.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);
	glTexCoord2f(1.0f, 0);
	glVertex2f(1.0f, -1.0f);
	glEnd();
}

void uRectOnScreenFFP(float rsX, float rsY) {
	//DON'T USE THIS IN YOUR CODE THIS IS NOT GOOD//
	float a = 1920.0f / rsX;
	float b = 1080.0f / rsY;
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(-1.0f, -1.0f);
	glTexCoord2f(0, b);
	glVertex2f(-1.0f, 1.0f);
	glTexCoord2f(a, b);
	glVertex2f(1.0f, 1.0f);
	glTexCoord2f(a, 0);
	glVertex2f(1.0f, -1.0f);
	glEnd();
}

void uDrawCubeFFP(float x, float y, float z) {
	//DON'T USE THIS IN YOUR CODE THIS IS NOT GOOD//
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1);

	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(x + 0.0f, y + 0.0f, z + 0.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 1.0f, y + 0.0f, z + 0.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 1.0f, y + 1.0f, z + 0.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 0.0f, y + 1.0f, z + 0.0f); glTexCoord2f(0, 1);

	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(x + 0.0f, y + 0.0f, z + -1.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 1.0f, y + 0.0f, z + -1.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 1.0f, y + 1.0f, z + -1.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 0.0f, y + 1.0f, z + -1.0f); glTexCoord2f(0, 1);

	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(x + 1.0f, y + 0.0f, z + 0.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 1.0f, y + 0.0f, z + -1.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 1.0f, y + 1.0f, z + -1.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 1.0f, y + 1.0f, z + 0.0f); glTexCoord2f(0, 1);

	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(x + 0.0f, y + 0.0f, z + 0.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 0.0f, y + 0.0f, z + -1.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 0.0f, y + 1.0f, z + -1.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 0.0f, y + 1.0f, z + 0.0f); glTexCoord2f(0, 1);

	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x + 0.0f, y + 1.0f, z + 0.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 1.0f, y + 1.0f, z + 0.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 1.0f, y + 1.0f, z + -1.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 0.0f, y + 1.0f, z + -1.0f); glTexCoord2f(0, 1);

	glNormal3f(0.0f, -1.0f, 0.0f);
	glVertex3f(x + 0.0f, y + 0.0f, z + 0.0f); glTexCoord2f(0, 0);
	glVertex3f(x + 1.0f, y + 0.0f, z + 0.0f); glTexCoord2f(1, 0);
	glVertex3f(x + 1.0f, y + 0.0f, z + -1.0f); glTexCoord2f(1, 1);
	glVertex3f(x + 0.0f, y + 0.0f, z + -1.0f); glTexCoord2f(0, 1);
	glNormal3f(0.0f, 0.0f, -1.0f);
	glDisable(GL_TEXTURE_2D);
	glEnd();
}

void uDrawOctahedronFFP(const float &x, const float &y, const float &z, const float &s) {
	glBegin(GL_TRIANGLES);
	glVertex3f(0 + x, s + y, 0 + z);
	glVertex3f(s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, s + z);

	glVertex3f(0 + x, s + y, 0 + z);
	glVertex3f(-s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, s + z);

	glVertex3f(0 + x, -s + y, 0 + z);
	glVertex3f(s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, s + z);

	glVertex3f(0 + x, -s + y, 0 + z);
	glVertex3f(-s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, s + z);

	glVertex3f(0 + x, s + y, 0 + z);
	glVertex3f(s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, -s + z);

	glVertex3f(0 + x, s + y, 0 + z);
	glVertex3f(-s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, -s + z);

	glVertex3f(0 + x, -s + y, 0 + z);
	glVertex3f(s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, -s + z);

	glVertex3f(0 + x, -s + y, 0 + z);
	glVertex3f(-s + x, 0 + y, 0 + z);
	glVertex3f(0 + x, 0 + y, -s + z);
	glEnd();
}

GLuint uLoadImage(const char* imagepath) {
	int anisotropicSamples = 4; //TODO - Put this in settings file

	ILuint imageID;    //For ILU (DevIL) to use
	GLuint textureID;  //For OpenGL to use
	ILboolean success;
	ILenum error;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	success = ilLoadImage(imagepath);
	//If we managed to load the image, then we can start to do things with it...
	if (success){
		//If the image is flipped (i.e. upside-down and mirrored, flip it the right way up!)
		ILinfo ImageInfo;
		iluGetImageInfo(&ImageInfo);
		if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT){
			iluFlipImage();
		}
		//Convert the image into a suitable format to work with
		//NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
		//Quit out if we failed the conversion
		if (!success){
			error = ilGetError();
			std::cerr << "Opening Image from " << imagepath << "failed! ILU error info-- " << error << " - " << iluErrorString(error) << std::endl;
			return 0;
		}
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D, textureID);

		//Set textures to repeat if uv.x/y is < 0.0 || > 1.0
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		//Set how OpenGL displays the texture on zomming in/out.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//Set how textures look at an angle.
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropicSamples);

		//Give texture data to OpenGL and make the mipmaps.
		glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH),	ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);
	} else {
		error = ilGetError();
		std::cerr << "Opening Image from " << imagepath << "failed! ILU error-- " << error << " - " << iluErrorString(error) << std::endl;
		return 0;
	}
	ilDeleteImages(1, &imageID);
	std::cout << "Image loaded from " << imagepath << std::endl;
	return textureID;
}

GLuint uLoadBMP(const char* imagepath) {
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int width, height;
	unsigned int imageSize;
	unsigned char * data;
	std::clog << "Loading BMP texture from " << imagepath << "..." << std::endl;
	FILE * file = fopen(imagepath, "rb");
	if(!file) { 
		fprintf(stderr, "Opening Image %s failed!\n",imagepath);
		return -1;
	}
	if(fread(header, 1, 54, file) != 54) {
		fprintf(stderr, "Not a 24-bit BMP file!\n");
		return -1;
	}
	if(header[0] != 'B' || header[1] != 'M') {
		fprintf(stderr, "Not a 24-bit BMP file!\n");
		return -1;
	}
	dataPos = *(int*) &(header[0x0A]);
	imageSize = *(int*) &(header[0x22]);
	width = *(int*) &(header[0x12]);
	height = *(int*) &(header[0x16]);
	if (imageSize == 0) imageSize = width*height*3;
	if (dataPos == 0) dataPos = 54;
	data = new unsigned char[imageSize];
	fread(data, 1, imageSize, file);
	fclose(file);

	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	glGenerateMipmap(GL_TEXTURE_2D);

	std::clog << "Loaded texture successfully!\n" << std::endl;

	return textureID;
}

GLuint uLoadBMPCubemap(const char* folderpath) {
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	for (GLuint i = 0; i < 6; i++) {
		unsigned char header[54];
		unsigned int dataPos;
		unsigned int width, height;
		unsigned int imageSize;
		unsigned char * data;
		std::stringstream imagepath;
		imagepath << folderpath << "/" << std::to_string(i) + ".bmp";
		std::clog << "Loading BMP texture from " << imagepath.str() << "..." << std::endl;
		FILE * file = fopen(imagepath.str().c_str(), "rb");
		if (!file) {
			fprintf(stderr, "Opening Image %s failed!\n", imagepath);
			return -1;
		}
		if (fread(header, 1, 54, file) != 54) {
			fprintf(stderr, "Not a 24-bit BMP file!\n");
			return -1;
		}
		if (header[0] != 'B' || header[1] != 'M') {
			fprintf(stderr, "Not a 24-bit BMP file!\n");
			return -1;
		}
		dataPos = *(int*)&(header[0x0A]);
		imageSize = *(int*)&(header[0x22]);
		width = *(int*)&(header[0x12]);
		height = *(int*)&(header[0x16]);
		if (imageSize == 0) imageSize = width*height * 3;
		if (dataPos == 0) dataPos = 54;
		data = new unsigned char[imageSize];
		fread(data, 1, imageSize, file);
		fclose(file);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	}

	//Note - no mipmaps here, apparently they glitch things up with cubemaps (I think, not sure).
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	std::clog << "Loaded textures successfully!\n" << std::endl;

	return textureID;
}

std::string uReadFile(const char *filePath) {
	std::string content;
	std::ifstream fileStream(filePath, std::ios::in);

	if (!fileStream.is_open()) {
		std::cerr << "Could not read file " << filePath << ". File does not exist." << std::endl;
		return "";
	}

	std::string line = "";
	while (!fileStream.eof()) {
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

GLuint uLoadShader(std::string folder_path) {
	return uLoadShader((folder_path + "/V.shader").c_str(), (folder_path + "/F.shader").c_str());
}

GLuint uLoadShader(const char* vertex_path, const char* fragment_path) {
	std::clog << "Loading Vertex & Fragment shaders from " << vertex_path << " & " << fragment_path << std::endl;

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	
	std::string vertShaderStr = uReadFile(vertex_path);
	std::string fragShaderStr = uReadFile(fragment_path);
	const char *vertShaderSrc = vertShaderStr.c_str();
	const char *fragShaderSrc = fragShaderStr.c_str();

	GLint result = GL_FALSE;
	int logLength;

	std::clog << "Compiling vertex shader...";
	glShaderSource(vertShader, 1, &vertShaderSrc, NULL);
	glCompileShader(vertShader);

	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<char> vertShaderError(logLength);
	if (logLength > 0) {
		glGetShaderInfoLog(vertShader, logLength, NULL, &vertShaderError[0]);
		std::cerr << &vertShaderError[0] << std::endl;
	}

	std::clog << "Compiling fragment shader...";
	glShaderSource(fragShader, 1, &fragShaderSrc, NULL);
	glCompileShader(fragShader);

	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<char> fragShaderError(logLength);
	if (logLength > 0) {
		glGetShaderInfoLog(fragShader, logLength, NULL, &fragShaderError[0]);
		std::cerr << &fragShaderError[0] << std::endl;
	}

	std::clog << "Linking program...";
	GLuint program = glCreateProgram();
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	for (int i = 0; i < 16; i++) {
		std::ostringstream oss;
		oss << "uStream" << i;
		std::string s = oss.str();
		glBindAttribLocation(program, i, s.c_str());
	}
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<char> programError((logLength > 1) ? logLength : 1);
	//glGetProgramInfoLog(program, logLength, NULL, &programError[0]);
	//std::cout << &programError[0] << std::endl;

	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	glUseProgram(program);
	uSetUniform1i(program, "TA", 0);
	uSetUniform1i(program, "TB", 1);
	uSetUniform1i(program, "TC", 2);
	uSetUniform1i(program, "TD", 3);
	uSetUniform1i(program, "TE", 4);
	uSetUniform1i(program, "TF", 5);
	uSetUniform1i(program, "TG", 6);
	uSetUniform1i(program, "TH", 7);
	uSetUniform1i(program, "TI", 8);
	uSetUniform1i(program, "TJ", 9);
	uSetUniform1i(program, "TK", 10);
	uSetUniform1i(program, "TL", 11);
	uSetUniform1i(program, "TM", 12);
	uSetUniform1i(program, "TN", 13);
	uSetUniform1i(program, "TO", 14);
	uSetUniform1i(program, "TP", 15);
	uSetUniform1i(program, "TQ", 16);

	std::clog << "Shaders loaded successfully!\n" << std::endl;

	return program;
}

void uRenderToFBO(int FBOID) {
	glBindFramebuffer(GL_FRAMEBUFFER, FBOID);
}

void uPrintCurrentProgram() {
	GLint prgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prgram);
	std::clog << "Current Program: " << prgram << std::endl;
}

void uFilterDepthWrite(int FBOID, int tex) {
	uRenderToFBO(FBOID);
	glClear(GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, tex);
	uRectOnScreenFFPWriteDepth();
}

void uFilterFBOToFBO(int FBOID, int shader, int xres, int yres, int texturesToUse, ...) {
	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	uRenderToFBO(FBOID);
	glViewport(0, 0, xres, yres);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader);
	
	uSetUniform2fNoSwitch("RES", xres, yres);

	va_list arguments;
	va_start(arguments, texturesToUse);
	for (int x = 0; x < texturesToUse; x++) {
		glActiveTexture(GL_TEXTURE0 + x);
		glBindTexture(GL_TEXTURE_2D, va_arg(arguments, GLuint));
	}
	va_end(arguments);

	uRectOnScreenFFP();

	glActiveTexture(GL_TEXTURE0);
}

void uFilterFBOToFBONoClear(int FBOID, int shader, int xres, int yres, int texturesToUse, ...) {
	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	uRenderToFBO(FBOID);
	glViewport(0, 0, xres, yres);
	glUseProgram(shader);

	uSetUniform2fNoSwitch("RES", xres, yres);

	va_list arguments;
	va_start(arguments, texturesToUse);
	for (int x = 0; x < texturesToUse; x++) {
		glActiveTexture(GL_TEXTURE0 + x - 1);
		glBindTexture(GL_TEXTURE_2D, va_arg(arguments, GLuint));
	}
	va_end(arguments);

	uRectOnScreenFFP();

	glActiveTexture(GL_TEXTURE0);
}

void uFilterFBOToFBONoClear(int FBOID, int shader, int x0, int y0, int x1, int y1, int texturesToUse, ...) {
	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	uRenderToFBO(FBOID);
	glViewport(x0, y0, x1, y1);
	glUseProgram(shader);

	uSetUniform2fNoSwitch("RES", x1, y1);

	va_list arguments;
	va_start(arguments, texturesToUse);
	for(int x = 0; x < texturesToUse; x++) {
		glActiveTexture(GL_TEXTURE0 + x - 1);
		glBindTexture(GL_TEXTURE_2D, va_arg(arguments, GLuint));
	}
	va_end(arguments);

	uRectOnScreenFFP();

	glActiveTexture(GL_TEXTURE0);
}

glm::vec3 uRoundVecTo(glm::vec3 v, float divisionNum) {
	v.x = round(v.x / divisionNum) * divisionNum;
	v.y = round(v.y / divisionNum) * divisionNum;
	v.z = round(v.z / divisionNum) * divisionNum;
	return v;
}

glm::vec3 uMul3x4(glm::vec3 p, glm::mat4 matrix) {
	return glm::vec3(glm::vec4(p, 1.0) * matrix);
}

glm::mat4 uRotateToPoint(glm::vec3 O, glm::vec3 P, glm::vec3 U) {
	glm::vec3 D = (O - P);
	glm::vec3 right = glm::cross(U, D);
	right = glm::normalize(right);
	glm::vec3 backwards = glm::cross(right, U);
	backwards = glm::normalize(backwards);
	glm::vec3 up = glm::cross(backwards, right);
	glm::mat4 rot = glm::mat4(right.x, right.y, right.z, 0, up.x, up.y, up.z, 0, backwards.x, backwards.y, backwards.z, 0, 0, 0, 0, 1);
	return rot;
}