//@author Adam Lastowka

//uMaterial is a class designed to store information about a certain material (like a blender material).
//In addition to storing textures and diffuse/specular intensity and color data, a uMaterial can also apply() itself, making it the currently active material.


#include "uMaterial.h"

#include <GL/glew.h>  
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <iostream>

#include "GLUtil.h"

uMaterial::uMaterial() {
	
}

void uMaterial::apply() {
	if (use_albedo_texture) {
		glDisable(GL_TEXTURE0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, albedo_texture);
		uSetUniform1fNoSwitch("albedo_texture_on", 1.f);
	} else {
		glDisable(GL_TEXTURE0);
		uSetUniform1fNoSwitch("albedo_texture_on", 0.f);
	}

	if (use_ambient_texture) {
		glEnable(GL_TEXTURE1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, ambient_texture);
		uSetUniform1fNoSwitch("ambient_texture_on", 1.f);
	}
	else {
		glDisable(GL_TEXTURE1);
		uSetUniform1fNoSwitch("ambient_texture_on", 0.f);
	}

	if (use_normal_texture) {
		glEnable(GL_TEXTURE2);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, normal_texture);
		uSetUniform1fNoSwitch("normal_texture_on", 1.f);
	} else {
		glDisable(GL_TEXTURE2);
		uSetUniform1fNoSwitch("normal_texture_on", 0.f);
	}
	glActiveTexture(GL_TEXTURE0);

	if (do_reflections || reflection_intensity == 0.f) {
		glEnable(GL_TEXTURE3);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_CUBE_MAP, reflection_cubemap);
		uSetUniform1fNoSwitch("do_reflections", 1.f);
	} else {
		glDisable(GL_TEXTURE3);
		uSetUniform1fNoSwitch("do_reflections", 0.f);
	}

	uSetUniform1fNoSwitch("ambient_intensity", ambient_intensity);

	uSetUniform1fNoSwitch("specular_intensity", specular_intensity);
	uSetUniform3fNoSwitch("specular_color", specular_color);
	uSetUniform1fNoSwitch("hardness", hardness);

	uSetUniform1fNoSwitch("diffuse_intensity", diffuse_intensity);
	uSetUniform3fNoSwitch("diffuse_color", diffuse_color);

	uSetUniform1fNoSwitch("reflection_intensity", reflection_intensity);
	glEnable(GL_TEXTURE0);
}

void uMaterial::printInfo() {
	std::clog << "Printing information of material " << name << std::endl;
	std::clog << "     use_albedo_texture - " << use_albedo_texture << std::endl;
	std::clog << "     use_ambient_texture - " << use_ambient_texture << std::endl;
	std::clog << "     use_normal_texture - " << use_normal_texture << std::endl;
	std::clog << "     do_reflections - " << do_reflections << std::endl;
	if (use_albedo_texture)
		std::clog << "         albedo texture - " << albedo_texture << std::endl;
	if (use_ambient_texture)
		std::clog << "         ambient texture - " << ambient_texture << std::endl;
	if (use_normal_texture)
		std::clog << "         normal texture - " << normal_texture << std::endl;
	if (do_reflections)
		std::clog << "         reflection cubemap texture - " << reflection_cubemap << std::endl;
	std::clog << "      ambient_intensity - " << ambient_intensity << std::endl;
	std::clog << "      diffuse_intensity - " << diffuse_intensity << std::endl;
	std::clog << "          diffuse_color - " << "(" << diffuse_color.x << ", " << diffuse_color.y << ", " << diffuse_color.z << ")" << std::endl;
	std::clog << "     specular_intensity - " << specular_intensity << std::endl;
	std::clog << "         specular_color - " << "(" << specular_color.x << ", " << specular_color.y << ", " << specular_color.z << ")" << std::endl;
	std::clog << "     hardness - " << hardness << std::endl;
	std::clog << "     reflection_intensity - " << std::endl;
	std::clog << "          reflection_color - " << "(" << reflection_color.x << ", " << reflection_color.y << ", " << reflection_color.z << ")" << std::endl;
}
void uMaterial::printInfoDebug() {
	std::cout << "Printing information of material " << name << std::endl;
	std::cout << "     use_albedo_texture - " << use_albedo_texture << std::endl;
	std::cout << "     use_ambient_texture - " << use_ambient_texture << std::endl;
	std::cout << "     use_normal_texture - " << use_normal_texture << std::endl;
	std::cout << "     do_reflections - " << do_reflections << std::endl;
	if (use_albedo_texture)
		std::cout << "         albedo texture - " << albedo_texture << std::endl;
	if (use_ambient_texture)
		std::cout << "         ambient texture - " << ambient_texture << std::endl;
	if (use_normal_texture)
		std::cout << "         normal texture - " << normal_texture << std::endl;
	if (do_reflections)
		std::cout << "         reflection cubemap texture - " << reflection_cubemap << std::endl;
	std::cout << "      ambient_intensity - " << ambient_intensity << std::endl;
	std::cout << "      diffuse_intensity - " << diffuse_intensity << std::endl;
	std::cout << "          diffuse_color - " << "(" << diffuse_color.x << ", " << diffuse_color.y << ", " << diffuse_color.z << ")" << std::endl;
	std::cout << "     specular_intensity - " << specular_intensity << std::endl;
	std::cout << "         specular_color - " << "(" << specular_color.x << ", " << specular_color.y << ", " << specular_color.z << ")" << std::endl;
	std::cout << "     hardness - " << hardness << std::endl;
	std::cout << "     reflection_intensity - " << std::endl;
	std::cout << "          reflection_color - " << "(" << reflection_color.x << ", " << reflection_color.y << ", " << reflection_color.z << ")" << std::endl;
}