//----------------------------------------------------------------//
//                        ADAM LASTOWKA                           //
//                      AutumnLight Engine                        //
//----------------------------------------------------------------//

//The highest level in the program. Contains main() and 'runs' Tesseract.cpp.
// Also currently handles the log teeing.

#include <GLFW/glfw3.h>
#include "Tesseract.h"
#include "miscUtil.h"

#include <fstream>
#include <ostream>
#include "teeS.h"

bool ERR_TO_CON = true;  // std::cerr // This should probably be always on.
bool LOG_TO_CON = false;  // std::clog
bool OUT_TO_CON = true;  // std::cout

int main() {
	// urrrghh, this code seems to really like being here, and dosen't work anywhere else.
	std::ostream console (std::cout.rdbuf()); //make a ostream to hold the console stream
	console.rdbuf(std::cout.rdbuf()); // assign the console stream to var console
	std::ofstream logf("log.txt");  // The log file stream
	teestream teeout(logf, console); //make a tee to the console and the logfile
	if (ERR_TO_CON)
		std::cerr.rdbuf(teeout.rdbuf()); //redirect cerr to the tee
	else
		std::cerr.rdbuf(logf.rdbuf());

	if (LOG_TO_CON)
		std::clog.rdbuf(teeout.rdbuf()); //redirect clog to the tee
	else
		std::clog.rdbuf(logf.rdbuf());

	if (OUT_TO_CON)
		std::cout.rdbuf(teeout.rdbuf()); //redirect cout to the tee
	else
		std::cout.rdbuf(logf.rdbuf());

	std::clog << dateTime24().c_str();
	initialize();
	do {
		loop();
	} while (!shouldClose());
	killWindow();
	return 1;
}

#include <stdio.h>
#include <stdarg.h>

int tfprintf(FILE *fpa, FILE *fpb, const char *fmt, ...)
{
	int rc = 0;
	va_list ap = { 0 };
	va_start(ap, fmt);
	rc = vfprintf(fpa, fmt, ap);
	va_end(ap);
	if (rc >= 0)
	{
		va_start(ap, fmt);
		rc = vfprintf(fpb, fmt, ap);
		va_end(ap);
	}
	return rc;
}
