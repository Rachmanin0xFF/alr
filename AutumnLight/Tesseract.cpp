
//AutumnLight Relativity Build
//Pre-Alpha v2.0
//@author Adam Lastowka

/* DOCUMENTATION //---------------------------------------------------//

G-BUFFER ARRAY ELEMENTS:
	[0] - F.B.O. I.D.
	[1] - Depth24 Stencil
	[2] - Albedo.rgb DiffuseIntensity
	[3] - Normal.xy specI hardness
	[4] - LightAccumulation.rgb unassigned

SHADER CUSTOMS:
	
	All Shaders should start with "#version 330 core" for compatibility reasons.
	
	VERTEX ATTRIBUTE STREAM INDICES:
		uStream0 - Positions
		uStream2 - Normals
		uStream8 - Texture Coordinates
		uStream9 - Tangent
		uStream10 - Bitangent

	GL_TEXTURE_X INDICES (during G-Buffer write):
		0 - TA - Albedo Texture
		1 - TB - Ambient Lighting Texture
		2 - TC - Normal Texture
		3 - TD -
		4 - TE -

	UNIFORMS:
		RES (vec2) - The resolution of the framebuffer target.
		projectionMatrix (mat4) - The eye's projection matrix.
		modelViewMatrix (mat3 / mat4) - The eye's modelView transformation matrix, computed as:
			viewMatrix * modelMatrix
		modelViewProjectionMatrix (mat4) - The eye's full visual transformation matrix, computed as:
			projMatrix * viewMatrix * modelMatrix
		normalMatrix (mat3) - The eye's normal modelView matrix, computed as:
			(mat3(viewMatrix * modelMatrix)^-1)^T

//--------------------------------------------------------------------*/
#include <GL/glew.h>  

#include <GLFW/glfw3.h>

#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <sstream>

#include <vector>

#include <time.h>
#include <math.h>

#include "GLUtil.h"
#include "FPSCounter.h"
#include "TestCamera.h"
#include "uScene.h"
#include "uMesh.h"


// System Variables //------------------------------------------------//

//
int deepTick = 0;
GLFWwindow* window;
FPSCounter FrameSpeedCounter;

// Settings //--------------------------------------------------------//

//Screen Resolution X
int resSX = 0;
//Screen Resolution Y
int resSY = 0;
//Previous Screen Resolution X
int prsx = 0;
//Previous Screen Resolution Y
int prsy = 0;

// OpenGL Variables //------------------------------------------------//
TestCamera eyeCam;
glm::mat4x4 projectionMatrix;
glm::mat4x4 viewMatrix;
glm::mat4x4 modelMatrix;

// FrameBuffer Objects //--------------------------------------------//
GLuint testFBO[5];


// Core Objects //---------------------------------------------------//
GLuint sdPlain;
GLuint sdWrite;
GLuint sdWriteStencil;
GLuint sdBorderlands;

GLuint cubemapTestTex;
GLuint whiteTex;
GLuint blackTex;
GLuint colorTestTex;
GLuint randomTex;


// Map data //--------------------------------------------------------//
// The primary scene
uScene mainScene("Main");
std::vector<std::shared_ptr<uModel>> uniModels;
//--------------------------------------------------------------------//
void deleteFBOS();
void remakeFBOS();
void renderGL(int mode);
void clipPlane(int index, glm::vec3 pos, glm::vec3 norm);
void clipPlaneOff(int index);
void applyTransformations();
void setUpGL();
bool shouldClose();
void killWindow();
//--------------------------------------------------------------------//

uMesh stencilTesting;

int wasIconified = 1;
void deleteFBOS() {
	glDeleteFramebuffers(1, &testFBO[0]);
	glDeleteTextures(1, &testFBO[1]);
	glDeleteTextures(1, &testFBO[2]);
	glDeleteTextures(1, &testFBO[3]);
	glDeleteTextures(1, &testFBO[4]);
}

void remakeFBOS() {
	glfwGetWindowSize(window, &resSX, &resSY);
	deleteFBOS();

	//uNewFBO_FCrDSr(testFBO, resSX, resSY);
	uNewGBuffer(testFBO, resSX, resSY, false, false);
}

void keeeb(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_H)
			uRoom::debugPrint = true;
		if (key == GLFW_KEY_F1)
			mainScene.rooms[0]->visible = !mainScene.rooms[0]->visible;
		if (key == GLFW_KEY_F2)
			mainScene.rooms[1]->visible = !mainScene.rooms[1]->visible;
		if (key == GLFW_KEY_F3)
			mainScene.rooms[2]->visible = !mainScene.rooms[2]->visible;
		if (key == GLFW_KEY_G) {
			uRoom::debugBool = !uRoom::debugBool;
		}
	}
	else {

	}
}

/*Called on program startup.*/
void initialize() {
	glfwSwapInterval(1);
	//--------------------------------------------------------------------//
	fprintf(stdout, "/////CORE INITIALIZATION STARTED/////\n");
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLFW initialized successfully!\n");
	//window = glfwCreateWindow(1920, 1080, "AutumnLight", glfwGetPrimaryMonitor(), NULL);
	//window = glfwCreateWindow(1280, 720, "AutumnLight", glfwGetPrimaryMonitor(), NULL);
	//window = glfwCreateWindow(1600, 900, "AutumnLight", NULL, NULL);
	window = glfwCreateWindow(1280, 720, "AutumnLight", NULL, NULL);
	glfwGetWindowSize(window, &resSX, &resSY);
	if (!window) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLFW window created successfully!.\n");
	glfwMakeContextCurrent(window);
	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLEW initialized successfully!\n");

	ilInit();
	iluInit();
	ilutRenderer(ILUT_OPENGL);
	fprintf(stdout, "DevIL initialized successfully!\n");
	fprintf(stdout, "/////CORE INITIALIZATION COMPLETED/////\n\n");

	//--------------------------------------------------------------------//
	srand(time(NULL));
	setUpGL();
	projectionMatrix = glm::mat4();
	viewMatrix = glm::mat4();
	modelMatrix = glm::mat4();
	//--------------------------------------------------------------------//
	uNewGBuffer(testFBO, resSX, resSY, false, false);

	sdPlain = uLoadShader("data/glsl/plain");
	sdWrite = uLoadShader("data/glsl/write");
	sdWriteStencil = uLoadShader("data/glsl/writeStencil");
	sdBorderlands = uLoadShader("data/glsl/_fun/borderlands");

	stencilTesting.loadOBJRAW("data/light/icosahedronlight.obj", true);
	stencilTesting.material = std::make_shared<uMaterial>();
	stencilTesting.material->diffuse_color = glm::vec3(0.0, 1.0, 0.0);
	/*cubemapTestTex = uLoadBMPCubemap("data/textures/cubemap");
	whiteTex = uLoadBMP("data/textures/white.bmp");
	blackTex = uLoadBMP("data/textures/black.bmp");
	colorTestTex = uLoadBMP("data/textures/colorTest.bmp");
	randomTex = uLoadBMP("data/textures/random.bmp");*/
	
	mainScene.loadFromFile("data/test_models/testWerld3/testWerld3.alm");
	mainScene.naught = mainScene.rooms[0];
	for (auto r : mainScene.rooms){
		r->printChilds(0);
	}
	//--------------------------------------------------------------------//
	prsx = resSX;
	prsy = resSY;
	std::cout << "Setup over. System online." << std::endl;
	glfwSetKeyCallback(window, keeeb);
	//glfwSetKeyCallback(window, key_callback);
}

float m_a;
float m_b;
float m_c;

/**
Called continuously while program runs.
*/
void loop() {
	//--------------------------------------------------------------------//
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	uRenderToFBO(testFBO[0]);
	glfwGetWindowSize(window, &resSX, &resSY);
	if (prsx != resSX || prsy != resSY) {
		prsy = resSY;
		remakeFBOS();
		prsx = resSX;
	}
	glViewport(0, 0, resSX, resSY);

	FrameSpeedCounter.tick();

	std::stringstream sstitle;
	//sstitle << " Avg ms: " << std::to_string(FrameSpeedCounter.msPerFrame);
	//sstitle << " Avg Dev of ms: " << std::to_string(FrameSpeedCounter.msDev);
	//sstitle << "POS: " << eyeCam.getDirection()[0] << " " << eyeCam.getDirection()[1] << " " << eyeCam.getDirection()[2];
	sstitle << " FPS: " << std::to_string(FrameSpeedCounter.FPS);
	glfwSetWindowTitle(window, sstitle.str().c_str());

	// eyecam crashes whenever the window is minimized, and the frame when it reappears
	int iconified = glfwGetWindowAttrib(window, GLFW_ICONIFIED);
	if (!iconified&&!wasIconified){
		eyeCam.tick(window, static_cast<float>(resSX), static_cast<float>(resSY), FrameSpeedCounter.deltaTime);
	}
	wasIconified = iconified;

	viewMatrix = eyeCam.getViewMatrix();
	projectionMatrix = eyeCam.getProjMatrix();
	//--------------------------------------------------------------------//
	//Experimental area (stencil buffers and things)
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f); // Colour to clear the scene
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glUseProgram(sdWrite);
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_STENCIL_TEST);
	applyTransformations();
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_TEXTURE_CUBE_MAP);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTestTex);
	glActiveTexture(GL_TEXTURE0);
	uSetUniform3fNoSwitch("eyePos", eyeCam.getPos());
	//glPolygonMode(GL_FRONT, GL_LINE);
	GLenum DrawBuffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, DrawBuffers);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Not clearing is faster (a couple ms, actually)
	glStencilMask(~0);
	glDisable(GL_SCISSOR_TEST);
	glClear(GL_STENCIL_BUFFER_BIT);
	//testRoom1.drawTough();
	//testRoom1.drawPortable(glm::quat(glm::vec3(0.0, 0.0, glfwGetTime()*-1.0)));
	if (glfwGetKey(window, GLFW_KEY_GRAVE_ACCENT) == GLFW_PRESS)
		uRoom::maxDepth = 0;//mainScene.renderSpec(0);
	else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
		uRoom::maxDepth = 1;//mainScene.renderSpec(1);
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
		uRoom::maxDepth = 2;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
		uRoom::maxDepth = 3;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
		uRoom::maxDepth = 4;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
		uRoom::maxDepth = 5;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)
		uRoom::maxDepth = 6;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS)
		uRoom::maxDepth = 7;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS)
		uRoom::maxDepth = 8;//mainScene.renderSpec(2);
	else if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS)
		uRoom::maxDepth = 9;//mainScene.renderSpec(2);

	//mainScene.render(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS);
	mainScene.renderPrimary();
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	//glPolygonMode(GL_FRONT, GL_FILL);
		
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_EQUAL, 1, 0xFF); // Pass test if stencil value is 1
	glStencilMask(0x00); // Don't write anything to stencil buffer
	glDepthMask(GL_TRUE);
	/*matrixScale(glm::vec3(1000.f, 1000.f, 1000.f));
	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	stencilTesting.material->apply();
	stencilTesting.display();
	matrixScale(glm::vec3(.001f, .001f, .001f));*/
	glDisable(GL_STENCIL_TEST);
	glCullFace(GL_BACK);
	
	glDisable(GL_STENCIL_TEST);
	glDisable(GL_CULL_FACE);

	//Change the sdWhatever thing here to switch postprocess shaders (filtering)
	uFilterFBOToFBONoClear(0, sdPlain, 0, 0, resSX, resSY, 1, testFBO[3]);
	
	/*uFilterFBOToFBONoClear(0, sdPlain, 0, 0, resSX / 2, resSY / 2, 1, testFBO[1]); //Bottom Left             |. |
	uFilterFBOToFBONoClear(0, sdPlain, resSX / 2, 0, resSX / 2, resSY / 2, 1, testFBO[2]);//Bottom Right     | .|
	uFilterFBOToFBONoClear(0, sdPlain, 0, resSY / 2, resSX / 2, resSY / 2, 1, testFBO[3]);//Top Left         |` |
	uFilterFBOToFBONoClear(0, sdPlain, resSX / 2, resSY / 2, resSX / 2, resSY / 2, 1, testFBO[4]);//Top Right| `|
	*/

	//--------------------------------------------------------------------//
	glfwSwapInterval(1);
	glfwSwapBuffers(window);
	glfwPollEvents();
	deepTick++;
	//--------------------------------------------------------------------//
}



//Mode:
//0: G-Buffer writing
//1: Shadow maps
void renderGL() {
}

void matrixTranslate(glm::vec3 position) {
	modelMatrix = glm::translate(modelMatrix, position);
}

void matrixRotate(glm::quat quat) {
	modelMatrix = modelMatrix*glm::mat4_cast(quat);
}

void matrixScale(glm::vec3 scale) {
	modelMatrix = glm::scale(modelMatrix, scale);
}

bool compareMatrix(glm::mat4 matA, glm::mat4 matB){
	double epsilon = 0.0001;
	return matA[0][0] - matB[0][0] < epsilon
		&& matA[0][1] - matB[0][1] < epsilon
		&& matA[0][2] - matB[0][2] < epsilon
		&& matA[0][3] - matB[0][3] < epsilon
		&& matA[1][0] - matB[1][0] < epsilon
		&& matA[1][1] - matB[1][1] < epsilon
		&& matA[1][2] - matB[1][2] < epsilon
		&& matA[1][3] - matB[1][3] < epsilon
		&& matA[2][0] - matB[2][0] < epsilon
		&& matA[2][1] - matB[2][1] < epsilon
		&& matA[2][2] - matB[2][2] < epsilon
		&& matA[2][3] - matB[2][3] < epsilon
		&& matA[3][0] - matB[3][0] < epsilon
		&& matA[3][1] - matB[3][1] < epsilon
		&& matA[3][2] - matB[3][2] < epsilon
		&& matA[3][3] - matB[3][3] < epsilon
		;
}
/**
Adds a clip plane with index GL_CLIP_PLANEx at position perpendicular to normal.
*/
void clipPlane(int index, glm::vec3 position, glm::vec3 normal) {
	glm::mat4 m = projectionMatrix*viewMatrix*modelMatrix;
	float mm[16] = { m[0][0], m[0][1], m[0][2], m[0][3], 
					m[1][0], m[1][1], m[1][2], m[1][3], 
					m[2][0], m[2][1], m[2][2], m[2][3], 
					m[3][0], m[3][1], m[3][2], m[3][3] };
	glLoadMatrixf(mm);

	double d = -(glm::dot(position, normal));
	//std::cerr << position.x << " " << position.y << " " << position.z << std::endl;
	glEnable(GL_CLIP_PLANE0 + index);
	double req[4] = { normal.x, normal.y, normal.z, d };
	glClipPlane(GL_CLIP_PLANE0 + index, req);
}

/**
Disables clip plane x.
*/
void clipPlaneOff(int index) {
	glDisable(GL_CLIP_PLANE0 + index);
	float mm[16] = { 1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0 };
	glLoadMatrixf(mm);
}

/**
Recalculates and sets shader matrices.
*/
void applyTransformations() {
	//Note-- passing mat4s to shaders instead of mat3s wil slow down performance (really, we do 4 multiplications that could be done with 3 components), is nonuniform scaling really worth it?
	uSetUniformMatrix4x4NoSwitch("modelViewProjectionMatrix", projectionMatrix * viewMatrix * modelMatrix);
	uSetUniformMatrix4x4NoSwitch("normalMatrix", glm::transpose(glm::inverse(viewMatrix * modelMatrix)));
	uSetUniformMatrix4x4NoSwitch("modelNormalMatrix", glm::transpose(glm::inverse(modelMatrix)));
	uSetUniformMatrix4x4NoSwitch("modelViewMatrix", viewMatrix * modelMatrix);
	uSetUniformMatrix4x4NoSwitch("modelViewMatrix", viewMatrix * modelMatrix);
	uSetUniformMatrix4x4NoSwitch("modelMatrix", modelMatrix);
}

/**
Sets the model matrix.
@param m: the matrix to set as the model matrix.
*/
void setModelMatrix(const glm::mat4x4 &m) {
	modelMatrix = m;
}

glm::mat4x4 getModelMatrix() {
	return modelMatrix;
}

void setUpGL() {
	//Enable multitexturing.
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE0);
	glEnable(GL_TEXTURE1);
	glEnable(GL_TEXTURE2);
	glEnable(GL_TEXTURE3);
	glEnable(GL_TEXTURE4);
	glEnable(GL_TEXTURE5);
	glEnable(GL_TEXTURE6);
	glEnable(GL_TEXTURE7);
	glEnable(GL_DEPTH_TEST);
	//Enable smooth shading
	glShadeModel(GL_SMOOTH);
	//Set active texture to 0
	glActiveTexture(GL_TEXTURE0);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
}

bool shouldClose() {
	return(glfwWindowShouldClose(window)) != 0;
}

void killWindow() {
	deleteFBOS();
	//glfwDestroyWindow(window);
	glfwTerminate();
}