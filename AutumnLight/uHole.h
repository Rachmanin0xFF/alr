
#ifndef UHOLE_H
#define UHOLE_H

#include "uObject.h"
#include "uModel.h"

#include <vector>
#include "uMaterial.h"
#include "uMesh.h"
#include <string>
#include <map>
#include <glm/gtx/quaternion.hpp>
#include <memory>


// Custom uModel type designed to render to the stencilbuffer
class uHole :public uModel{
public:
	uHole();
	uHole(std::string modelName){ name = modelName; };
	std::shared_ptr<uHole> linkedHole;
	std::shared_ptr<uRef> ref;
	//std::vector<std::shared_ptr<uRef>> refChildren;

	void render(){ render("unknown"); };
	void render(std::string renderedBy);
	//void addChild(std::shared_ptr<uRef> child);
	glm::vec3 clipPlanePos();
	glm::vec3 clipPlaneNormal();
};
#endif