
#ifndef UMODEL_H
#define UMODEL_H

#include "uObject.h"

#include <vector>
#include "uMaterial.h"
#include "uMesh.h"
#include <string>
#include <map>
#include <glm/gtx/quaternion.hpp>
#include <memory>
#include "Fizz.h"


class uModel:public uObject {
public:
	// Flags for creation
	std::string F_usegeom;
	bool isStatic;
	Fizz::Hull simpleHull;
	std::vector<std::shared_ptr<uMesh>> meshes;
	uModel();
	uModel(std::string modelName);

	void update();
	void render();
	void render(std::string renderedBy);


	void addMesh(std::shared_ptr<uMesh> mesh);
	void addMesh(uMesh mesh);
	void addMeshes(std::vector<std::shared_ptr<uMesh>> mesh);
};
#endif