//@author Adam Lastowka

//uModel is a class designed to handle a full model (a set of meshes, each with individual materials).
//It uses and std::map to allow multiple uMeshes to have the same material without duplicating the material itself.
//(each material has a unique std::string name that it can be refrenced by in the map, like hashmaps in java or dictionaries in python)

#define _CRT_SECURE_NO_DEPRECATE

#include "uModel.h"
#include "uHole.h"

#include <vector>
#include "uMaterial.h"
#include "uMesh.h"
#include "GLUtil.h"
#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <algorithm>

#include <string>
//#include "miscUtil.h"
#include "Tesseract.h"

#include "uRoom.h"
#include "uFizz.h"
#include "uRef.h"
#include "uLight.h"

uModel::uModel() {
	name = "NONAME";
	visible = true;
}

uModel::uModel(std::string modelName) {
	name = modelName;
	visible = true;
}

/* Does nothing*/
void uModel::update(){
	//TODO implement delta functions for transforms
}

/*Renders itself, and calls render() for child objects*/
void uModel::render(){
	render("unspecified(called w/o name)");
}

bool keyP = true;
/*Renders itself, and calls render() for child objects*/
void uModel::render(std::string renderedBy) {
	//Backup old model matrix
	glm::mat4x4 backup = getModelMatrix();
	//Roatate then translate by rot quaternion and pos
	//std::cout << name << " rendered by: " << renderedBy << std::endl;
	//std::cerr << "Key P: " << keyP<<std::endl;
	//if (keyP){
	//	matrixTranslate(pos);
	//	/*std::cerr << "TEST:\n";
	//	uPrintMatrix(glm::translate(getModelMatrix(), pos), std::cerr);
	//	uPrintMatrix(getModelMatrix()*glm::translate(glm::mat4(), pos), std::cerr);*/
	//	//applyTransformations();

	//	matrixRotate(rot);
	//	//applyTransformations();

	//	matrixScale(scale);
	//}
	//else{
		setModelMatrix(getModelMatrix()*matrix());
	//}
	//setModelMatrix(glm::translate(getModelMatrix(), pos));
	//setModelMatrix(getModelMatrix()*glm::mat4_cast(rot));
	// RENDER SELF //
	//getModelMatrix
	applyTransformations();
	for (int i = 0; i < meshes.size(); i++) {
		//Display mesh and materials
		meshes[i]->material->apply();
		meshes[i]->display();
		//std::cout << "rendering: " << name << "(" << children.size()<< ") by: " << renderedBy << " mesh: " << i << " mtl: " << meshes[i].material.name << std::endl;
	}
	// RENDER CHILDREN //
	//TODO render everything, not just gChildren
	for (int i = 0; i < modelChildren.size(); i++) {
		//Display children meshes
		modelChildren[i]->render(name);
	}

	for (int i = 0; i < fizzChildren.size(); i++) {
		//Display children meshes
		fizzChildren[i]->render(name);
	}

	for (int i = 0; i < lightChildren.size(); i++) {
		//Display children meshes
		lightChildren[i]->render(name);
	}

	for (int i = 0; i < holeChildren.size(); i++) {
		//Display children meshes
		holeChildren[i]->render(name);
	}
	//Reset model matrix
	setModelMatrix(backup);
}



/**Adds the specified uMesh shared_ptr to the uModel*/
void uModel::addMesh(std::shared_ptr<uMesh> mesh){
	meshes.push_back(mesh);
}

void uModel::addMesh(uMesh mesh){
	std::shared_ptr<uMesh> mesh_shptr(new uMesh(mesh));
	//mesh_shptr.reset(&mesh);
	addMesh(mesh_shptr);
}

/**Adds the specified vector of uMesh pointers to the uModel*/
void uModel::addMeshes(std::vector<std::shared_ptr<uMesh>> mesh){
	meshes.insert(meshes.end(), mesh.begin(), mesh.end());
}

