#ifndef TESSERACT_H
#define TESSERACT_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>

class uModel;
extern GLuint sdWrite;
extern GLuint sdWriteStencil;
extern std::vector<std::shared_ptr<uModel>> uniModels;
void initialize();

void loop();

bool shouldClose();

void killWindow();

void applyTransformations();

void setModelMatrix(const glm::mat4x4 &m);

// Returns the current model matrix. 
glm::mat4x4 getModelMatrix();

/**
Translates the modelmatrix by a given vec3.
@param position the vector to translate the model matrix by.
*/
void matrixTranslate(glm::vec3 position);
/**
Rotates the model matrix by a given quaternion.
@param quat the quaternion to rotate the model matrix by.
*/
void matrixRotate(glm::quat quat);
/**
Scales the model matrix by a given vec3.
@param scale the vector to scale by.
*/
void matrixScale(glm::vec3 scale);

bool compareMatrix(glm::mat4 matA, glm::mat4 matB);
/**
Adds a clipping plane.
@param index the OpenGL index of the plane to add
@param position A location that the plane passes through
@param normal A vector perpendicular to the plane
*/
void clipPlane(int index, glm::vec3 position, glm::vec3 normal);

/**
Turns off a clipping plane.
@param index the index of the clipping plane to disable.
*/
void clipPlaneOff(int index);

#endif