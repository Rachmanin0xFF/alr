#ifndef USCENE_H
#define USCENE_H

#include <string>
#include <vector>

#include "almLoader.h"
#include "uRoom.h"
#include "Tesseract.h"

class uScene{
public:
	std::string name;
	std::vector<std::shared_ptr<uRoom>> rooms;
	std::shared_ptr<uRoom> naught;
	//int currentRoom = 0; //THIS IS NOT GUD SYSTEM PROBS

	uScene(){ name = "NONAME"; };
	uScene(std::string newName){ name = newName; };

	/*Loads an alm file.*/
	void loadFromFile(std::string path){ ALM::loadALMFile(rooms, path); };

	void renderPrimary() {
		//// TRANSFORMING TO BLENDER ////
		glm::mat4x4 backup = getModelMatrix();
		setModelMatrix(getModelMatrix()*glm::mat4_cast(glm::quat(.707, -.707, 0, 0)));
		applyTransformations();
		//// BLENDER AXIS BELOW HERE ////
		naught->renderNaught();
		//// RESET TO AL AXIS ////
		setModelMatrix(backup);

	}
	// TODO implement single room drawing. this is REALLY connected to portals, travel, etc.
	/*void render(std::string renderedBy,bool tempthing){
		uRoom::roomDepth = -1;
		uRoom::stencilVal = 0;
		renderSpec(currentRoom, tempthing);
	}
	void renderAll(std::string renderedBy){
		//// TRANSFORMING TO BLENDER ////
		glm::mat4x4 backup = getModelMatrix();
		setModelMatrix(getModelMatrix()*glm::mat4_cast(glm::quat(.707, -.707,0,0)));
		applyTransformations();
		//// BLENDER AXIS BELOW HERE ////
		for (auto rm : rooms){
			glm::mat4x4 backup2 = getModelMatrix();
			matrixTranslate(rm->pos);
			matrixRotate(rm->rot);
			matrixScale(rm->scale);
			applyTransformations();
			rm->render(renderedBy);
			setModelMatrix(backup2);
		}
		//// RESET TO AL AXIS ////
		setModelMatrix(backup);
	};
	void renderSpec(int num, bool tempthing){
		//// TRANSFORMING TO BLENDER ////
		glm::mat4x4 backup = getModelMatrix();
		if (tempthing) 3;
		setModelMatrix(getModelMatrix()*glm::mat4_cast(glm::quat(.707, -.707, 0, 0)));
		applyTransformations();
		//// BLENDER AXIS BELOW HERE ////


		auto rm = rooms[num];
		//std::cout << "Rendering room with name: " << rm->name<<std::endl;

		rm->render("meem");

		//// RESET TO AL AXIS ////
		setModelMatrix(backup);
	}
	void render(bool temp){ render(name,temp); }
	void update(){
		for (auto rm : rooms){
			rm->update();
		}
	}*/

};
#endif




