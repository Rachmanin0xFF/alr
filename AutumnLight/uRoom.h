#ifndef UROOM_H
#define UROOM_H

#include "uObject.h"
#include <string>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct uLink;//Declaration for use in uRoom

//A container to store a group of uObjects.
class uRoom : public uObject {
public:
	uRoom(){};
	uRoom(std::string newName){name = newName;};

	void update(){};
	//void render();
	//void render(std::string renderedBy);
	//void render(std::string renderedBy, int stencilVal);
	/**
	Renders the room to the screen as the initial room (Room Naught)
	*/
	void renderNaught();
	//*Note* renderTreeLevel is the level renderSub() is running at.
	//i.e. if called directly, it should be 0
	void renderSub(int renderTreeLevel, int SBval, int sbBitsUsed, std::shared_ptr<uHole> superHole);
	//Temp Global Variable
	static int roomDepth;
	//Array of stacks for each layer
	//static std::vector<uLink> stack[5];
	// Depth to render to
	static int maxDepth;
	static int stencilVal;
	static bool debugPrint;
	static int debugDepth;
	static bool debugBool;
};

struct uLink{
public:
	uLink(glm::mat4x4 roomMatrix, int roomStencilValue, std::shared_ptr<uRoom> room);
	glm::mat4x4 matrix;

	int stencilValue;
	std::shared_ptr<uRoom> linkedRoom;
};

// A global temporary storage device
//std::vector<uLink> stack[3];


#endif