// ALM Importer/Parser
#ifndef ALMLOADER_H
#define ALMLOADER_H

#include "uRoom.h"
#include "uMaterial.h"
#include <map>
#include <memory>

namespace ALM {
	/*Loads uRooms from an ALM file into @param &roomList */
	void loadALMFile(std::vector<std::shared_ptr<uRoom>> &roomList, std::string location);
	/*Tries to link holes with their counterparts*/
	void linkHoles(std::vector<std::shared_ptr<uRoom>> &roomList);
	void splitOnFirstSpace(std::string in, std::string& kwd, std::string& data);



	//std::map < std::string, tuple<Type,int> genAliasMap(std::vector<std::shared_ptr<uObject>> vec);
	//void loadALM(std::vector<std::shared_ptr<uModel>> &parentPointers, std::string location);
	std::map<std::string, std::shared_ptr<uMaterial>> loadMTL(std::string location);
	class flaggedObject {
	public:
		std::string Fparent;
		std::string Fgeom;
		std::string Fmtl;
		std::string Fsmooth;
		std::string name;
		std::string type;
		std::shared_ptr<uObject> ob;
		//std::shared_ptr<uMesh> mesh;
		flaggedObject();

	};
	class flaggedModel {
	public:
		flaggedModel();
		flaggedModel(
			uModel* modelPtr,
			std::string flag_parent,
			std::string flag_geom,
			std::string flag_mtl,
			std::string flag_smooth);

		uModel* model;
		std::string parent;
		std::string geom;
		std::string mtl;
		std::string smooth;
		void printAll();
	};
}
#endif