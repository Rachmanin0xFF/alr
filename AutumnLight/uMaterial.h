#ifndef UMATERIAL_H
#define UMATERIAL_H

#include <GL/glew.h>  
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>

#include <string>

class uMaterial {
public:
	std::string name;
	float specular_intensity = 0.0f;
	float diffuse_intensity = 0.0f;
	float ambient_intensity = 0.0f;
	float reflection_intensity = 0.5f; //no source in mtl files
	float hardness = 0.0f;
	glm::vec3 diffuse_color;
	glm::vec3 specular_color;
	glm::vec3 reflection_color; // not used yet
	bool use_albedo_texture = false;
	bool use_normal_texture = false;
	bool use_ambient_texture = false;
	bool do_smooth_shading = true;
	bool do_reflections = false;
	GLuint albedo_texture;
	GLuint ambient_texture;
	GLuint normal_texture;
	GLuint reflection_cubemap;

	/**
	Creates a new empty uMaterial.
	*/
	uMaterial();
	/**
	Sets the active material to the material this function is called from. Should be called when "write" (sdWrite) shader is active.
	*/
	void apply();
	/**
	Prints all material data to std::clog.
	*/
	void printInfo();
	/**
	Prints all material data to std::cout.
	*/
	void printInfoDebug();
};

#endif