

OpenGL G-Buffer Architecture

0 - FBOID

ID     R          G          B          A        FORMAT                 G-BUFFER GL TYPE
1    depth-----------------------    stencil     GL_DEPTH24_STENCIL8    Depth Stencil
2  albedo.r    albedo.g   albedo.b   diffuseI    GL_RGBA8               RT0
3  normal.x    normal.y    specI     hardness    GL_RGBA8 | GL_RGBA16F  RT1
4  LightAccumulationRGB-----------  unassigned   GL_RGBA8 | GL_RGBA16F  RT2