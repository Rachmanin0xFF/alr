
USING:
	-Screen-space decals (http://www.slideshare.net/blindrenderer/screen-space-decals-in-warhammer-40000-space-marine-14699854)
	-Deferred shading
		*Stencil buffer optimization optional
	-Face normal calculation in shader
	-SSAO (some form of it, probably custom)
	-Baked environment maps
	-Baked lightmap textures (radiosity)
	-Precalculated shadow maps
	-Lens flare / bloom
		*HDR optional
	-Chromatic abberation shaders
	-Glass as a postprocess (do it deferred-lighting-style) (lens flare on glass looks cool)
	-GPU particle systems
