So, the alm stuff is way harder than I thought. Mostly due to memory management.

We have decided to use pointers for most things, which is great and very efficient in terms of 
memory, but we forgot an important detail: all of those pointers point to objects, which have 
to be stored somewhere. My initial solution was to make a meshlib, a modellib, and a mtllib which
would store the objects and be stored in uRoom. However, during my work, I discovered a hidden 
problem: it is close to impossible to fill a vector of objects and at the same time hand out valid 
pointers to things.
These are pseudo-code for what I tried:

vector<uModel> modellib;
uModel model;

for line in file:
	// interpret stuff
	if line starts with "object":
		//save old object and make new one

		modellib.push_back(model) // This works fine. It creates a COPY of model in the vector

		// Method A:
		uModel* pointerToObject=&model //This creates a pointer to the variable model, and will return the last model handled, or randomness if it has been cleaned up

		//Method B:
		uModel* pointerToObject==&modellib.back //This gets a pointer of the last(most recent) object in modellib, which seems great. Unfortunitely, part of the vector being variable-sized is that it regularly (every time something is added) repositions itself in memory. The last pointer created will be valid, bit its the only one. Marginally better than option A, but unusable.


The possible solutions as I see them:

--- Solution W ---

pseudo-code:

vector<uModel> modellib;
vector.reserve(modelCount) //This reserves the space for models, meaning we can use option B and the memory won't rearrange
uModel model;
for line in file:
	// interpret stuff
	if line starts with "object":
		//save old object and make new one

		modellib.push_back(model) // This works fine. It creates a COPY of model in the vector

		//Method B:
		uModel* pointerToObject==&modellib.back 

- problems -
once we've added the allocated number of meshes, that's it, no more. if we go over, everything (EVERYTHING) breaks
if we're predefining the size, it'd be smarter and safer(less prone to memory shift) to use an array


--- solution X --- //Probably the best
use the vector libs, and just use indexes instead of pointers. pointers to the libs should be stable, so we can hand those out. each uModel and uMesh can have variables like[ vector<uMesh> *meshlib], and look up stuff in there.
- possible problem -
i have no idea how fast inexing is, but I'd guess pretty fast. considering our current system, this is probably fine

--- solution Y ---
no pointers, just use copies of everything.
- problems -
this isn't actually a full solution, as it dosen't fix the uModel storage to do parenting with. I guess we could just have uModels be litteral childern of other uModels
inflexible - cannot manipulate relationships in any way
inneficent - especially once meshes are being shared more often, it would really help to not duplicate identical meshes.
+This is probably the quickest to implement, although not by too much. Considering the work required, probably a bad idea.

--- solution Z ---
Magic








So those are my thought and observations on the topic.
-Benjamin