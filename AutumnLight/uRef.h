#ifndef UREF_H
#define UREF_H

#include "uObject.h"
#include <string>
#include "uRoom.h"
#include "uModel.h"

class uRef : public uObject {
public:
	uRef(){};
	uRef(std::string newName){ name = newName; };
	//std::string name; //Used for inter-room link forming
	std::string roomFlag;
	std::shared_ptr<uRoom> linkedRoom;

	//uRefStack getForStack(int stencilBufferVal, glm::mat4x4 matrix,std::shared_ptr<uRoom> room);
	// Applies transformations, and places a uRefStack on the stack
	//void placeOnStack(int stencilBufferVal);

	void update(){};
	void render(){ render("self::" + name); };
	void render(std::string renderedBy){};
	void render(std::string renderedBy, int stencilVal);
};

// A 


//class uRefStack {
//public:
//	uRefStack(){};
//	uRefStack(std::string newName){ name = newName; };
//	std::string name;
//	glm::mat4x4 matrix;
//
//	int stencilValue;
//	std::shared_ptr<uRoom> linkedRoom;
//
//
//	void render(){}; 
//	void render(std::string renderedBy);
//};
#endif