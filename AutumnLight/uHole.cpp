#define _CRT_SECURE_NO_DEPRECATE

#include "uModel.h"
#include "uHole.h"

#include <vector>
#include "uMaterial.h"
#include "uMesh.h"
#include "GLUtil.h"
#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <algorithm>

#include <string>
//#include "miscUtil.h"
#include "Tesseract.h"

#include "uRoom.h"
#include "uFizz.h"
#include "uRef.h"
#include "uLight.h"

//void uHole::addChild(std::shared_ptr<uRef> child){
//	refChildren.push_back(child);
//}

void uHole::render(std::string renderedBy) {
		//Backup old model matrix
		name;
		glm::mat4x4 backup = getModelMatrix();
		setModelMatrix(getModelMatrix()*matrix());
		applyTransformations();
		for (int i = 0; i < meshes.size(); i++) {
			//Display mesh and materials
			meshes[i]->material->apply();
			meshes[i]->display();
			//std::cout << "rendering: " << name << "(" << children.size()<< ") by: " << renderedBy << " mesh: " << i << " mtl: " << meshes[i].material.name << std::endl;
		}
		setModelMatrix(backup);
	/////// OLD //////
	/*
	// Hi Adam. Here's where I think we draw to the stencilBuffer
	// Here's the gist, as far as I can figure out:

	//Check to see if I'm visible, so I don't waste a stencilBuffer value
	bool isVisible = true;
	if (holeChildren.size() != 1) 1;// DEBUG FFF /// std::cerr << name << ": Not 1 uRef child\n";
	else {
		if (isVisible) {
			//If I'm visible, draw me to the stencilBuffer.
			//The stencilBuffer value is probably the size of the stack so far.
			//int stencilVal = uRoom::stack[uRoom::roomDepth].size();

			glUseProgram(sdWriteStencil);
			glm::mat4x4 backup = getModelMatrix();
			uRoom::stencilVal++;
			matrixTranslate(pos);
			matrixRotate(rot);
			matrixScale(scale);
			applyTransformations();

			// Save stencil filter values
			GLint stencilRef;
			glGetIntegerv(GL_STENCIL_REF, &stencilRef);
			//std::cout << "GL_STENCIL_REF=" << stencilRef << std::endl;

			glEnable(GL_STENCIL_TEST);
			glStencilFunc(GL_ALWAYS, uRoom::stencilVal, 0xFF);
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
			glStencilMask(0xFF);
			glDepthMask(GL_FALSE);
			//glDisable(GL_CULL_FACE);
			glEnable(GL_DEPTH_TEST);
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
			for (int i = 0; i < meshes.size(); i++) {
				meshes[i]->display();
			}
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glDepthMask(GL_TRUE);
			//glEnable(GL_CULL_FACE);
			glDisable(GL_STENCIL_TEST);

			glUseProgram(sdWrite);

			glEnable(GL_STENCIL_TEST);
			glStencilFunc(GL_EQUAL, stencilRef, 0xFF);
			glStencilMask(0x00);

			//Check for wether there is room (i.e. less than 255 things)
			//holeChildren[0]->render(name, uRoom::stencilVal);
			
			setModelMatrix(backup);
		}
	}*/
}
glm::vec3 uHole::clipPlanePos() {
	return glm::vec3(meshes[0]->stream[0][meshes[0]->faces[0].verts[0]]);// , meshes[0]->stream[0][meshes[0]->faces[0].verts[1]], meshes[0]->stream[0][meshes[0]->faces[0].verts[2]]);

}

glm::vec3 uHole::clipPlaneNormal() {
	glm::vec3 a = meshes[0]->stream[0][meshes[0]->faces[0].verts[0]];
	glm::vec3 b = meshes[0]->stream[0][meshes[0]->faces[0].verts[1]];
	glm::vec3 c = meshes[0]->stream[0][meshes[0]->faces[0].verts[2]];
	return glm::cross(a - c, b - c);
}
