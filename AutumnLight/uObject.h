
#ifndef UOBJECT_H
#define UOBJECT_H

#include <vector>
#include <string>
#include <glm/gtx/quaternion.hpp>
#include <memory>


class uModel;
class uFizz;
class uLight;
class uRef;
class uHole;

class uObject {
public:
	std::string name;
	std::string parentFlag="";
	bool visible=true;
	std::vector<std::shared_ptr<uModel>> modelChildren;
	std::vector<std::shared_ptr<uFizz>> fizzChildren;
	std::vector<std::shared_ptr<uLight>> lightChildren;
	std::vector<std::shared_ptr<uHole>> holeChildren;
	glm::vec3 pos;
	glm::quat rot;
	glm::vec3 scale=glm::vec3(1.0f);
	
	glm::mat4 matrix();
	virtual void update() = 0;
	//virtual void render() = 0;
	//virtual void render(std::string renderedBy)=0;
	/**Adds the specified uModel pointer to the uModel*/
	void addChild(std::shared_ptr<uModel> child);
	void addChild(std::shared_ptr<uFizz> child);
	void addChild(std::shared_ptr<uLight> child);
	void addChild(std::shared_ptr<uHole> child);
	void printChilds(int lvl);
	virtual ~uObject(){};
};

#endif