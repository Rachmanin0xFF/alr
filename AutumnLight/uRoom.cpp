#include "uRoom.h"
#include "almLoader.h"
#include <iostream>
#include "Tesseract.h"

#include <glm/gtc/matrix_transform.hpp>

#include "uRoom.h"
#include "uModel.h"
#include "uHole.h"
#include "uFizz.h"
#include "uRef.h"
#include "uLight.h"

#include "miscUtil.h"

#include "TestCamera.h"

#include "Fizz.h"
#include <algorithm>    // std::min

//Temp Global Variable
int uRoom::roomDepth=-1;

// Depth to render to
int uRoom::maxDepth = 0;

int uRoom::stencilVal = 0;

//CLEAN//std::vector<uLink> uRoom::stack[5] = { std::vector<uLink>(),std::vector<uLink>(), std::vector<uLink>(), std::vector<uLink>(), std::vector<uLink>() };
//#include <string>

struct RoomInstance {
	RoomInstance::RoomInstance(std::shared_ptr<uHole> hole) {
		init(Fizz::makePolyMesh(hole->meshes[0]),hole);
	}
	RoomInstance::RoomInstance(Fizz::PolyMesh holeMesh,std::shared_ptr<uHole> hole){ /*, std::shared_ptr<uRef> ref, std::shared_ptr<uRoom> roomPtr, int stencilBufferVal) {*/
		init(holeMesh, hole);
	}
private:
	void init(Fizz::PolyMesh holeMesh, std::shared_ptr<uHole> fhole) {
		mesh = holeMesh;
		hole = fhole;
		room = hole->ref->linkedRoom;
		localSpaceMatrix = glm::mat4x4()*hole->matrix()*hole->ref->matrix();
	}
public:
	std::shared_ptr<uRoom> room;
	glm::mat4x4 localSpaceMatrix;
	int sbVal;

	/*A PolyMesh for collision testing*/
	Fizz::PolyMesh mesh;
	std::shared_ptr<uHole> hole;
	/*A collection of models that intersect the hole*/
	std::vector<std::shared_ptr<uModel>> transGeom;

	void addTrans(std::shared_ptr<uModel> model) {
		transGeom.push_back(model);
	}
};
#include <bitset>
bool uRoom::debugPrint = true;
bool uRoom::debugBool = false;
int uRoom::debugDepth = 0;
void uRoom::renderNaught(){
	if (debugPrint) {
		for (int i = 0; i < debugDepth; i++) {
			std::cout << " | ";
		}
		std::cout << "@"<<name << "\n";
		uRoom::debugDepth++;
	}
	auto checkMat = getModelMatrix();
	std::vector<RoomInstance> roomStack;
	extern TestCamera eyeCam;

	//find relevant holes
	for (int i = 0; i < holeChildren.size(); i++){
		auto hole = holeChildren[i];
		auto holeMesh = Fizz::makePolyMesh(hole->meshes[0]); //TODO: Pregen these PolyMeshes
		if (Fizz::viewContains(eyeCam.getViewProjMatrix(), holeMesh)){
			roomStack.push_back(RoomInstance(holeMesh, hole));
		}
	}
	
	int sbBitCt = std::ceil(std::log2(roomStack.size() + 1));
	
	// Process child geom
	for (int i = 0; i < modelChildren.size(); i++){
		auto model = modelChildren[i];
		bool isTrans=false;

		if (model->isStatic){
			for (RoomInstance holeset : roomStack){
				//Optimization? : Check frustrum first
				if (Fizz::polyIntersection(holeset.mesh, model->simpleHull)){
					holeset.addTrans(model);
					isTrans = true;
				}
			}
		}
		if (!isTrans) {
			model->render();
		}
	}

	//Process holes
	glEnable(GL_STENCIL_TEST);
	glClear(GL_STENCIL);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	int possibleRoomCt = roomStack.size();
	if (sbBitCt > 8)possibleRoomCt = 255;
	//std::cout << "Holing for:" << name << "\n";
	for (int i = 0; i < possibleRoomCt; i++) {
		
		//auto roomInstance = roomStack[i];

		//add 1 because 0x00 is no hole
		roomStack[i].sbVal = (i + 1) << (8 - sbBitCt);
		
		//Verify SBval
		assert(roomStack[i].sbVal < 0xFF);
		//std::cout << "SBval=" << std::bitset<8> (roomStack[i].sbVal) <<" "<< roomStack[i].sbVal<< std::endl;

		glStencilFunc(GL_ALWAYS, roomStack[i].sbVal, 0xFF);
		glStencilMask(0xFF); // Write to stencil buffer
		
		
		if (!uRoom::debugBool) {
			glUseProgram(sdWriteStencil);				  //draw to SB
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
			glDepthMask(GL_FALSE); // Write to depth buffer //Unnecissary?
		}
		roomStack[i].hole->render();
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glUseProgram(sdWrite);
	}


	for (int i = 0; i < possibleRoomCt; i++) {
		//RoomInstance roomInstance = roomStack[i];
		//clear the depthBuffer for holes
		glStencilFunc(GL_EQUAL, roomStack[i].sbVal, 0xFF);
		glStencilMask(0x00);
		glDisable(GL_DEPTH_TEST); 
		//Depth testing was taken care of when the SB was drawn to
		//Draw to depth buffer, setting to all the way deep
		// (?'-')????.*???
		//Adam's Magic Here
		glEnable(GL_DEPTH_TEST);

		//Trans geometry
		clipPlane(0, roomStack[i].hole->clipPlanePos(), roomStack[i].hole->clipPlaneNormal());
		glDepthMask(GL_TRUE); //Unnecissary?
		glDisable(GL_STENCIL_TEST);

		//Render on this side
		for (auto geom : roomStack[i].transGeom) {
			geom->render();
		}

		assert("adam is this the right way to invert the clip plane? -B" && 1);
		clipPlane(0, roomStack[i].hole->clipPlanePos(), -roomStack[i].hole->clipPlaneNormal());
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_EQUAL, roomStack[i].sbVal, 0xFF); //Test for SBval in SB
		glStencilMask(0x00); //Don't write to SB

							 //Render on other side
		for (auto geom : roomStack[i].transGeom) {
			geom->render();
		}

		clipPlaneOff(0);
		if (debugPrint) {
			for (int i = 0; i < debugDepth; i++) {
				std::cout << " | ";
			}
			std::cout << "[" << roomStack[i].room->name<< "]\n";
		}
	}
	assert(checkMat == (getModelMatrix()));
	//process Subrooms
	if (sbBitCt <= 8) {
		for (int i = 0; i < possibleRoomCt; i++) {
			auto r = roomStack[i];
			auto modelMatrixBackup = getModelMatrix();
			setModelMatrix(getModelMatrix()*r.localSpaceMatrix);
			assert(r.hole->linkedHole); //TODO: Remove once hole-linking is stronger/tested
			r.room->renderSub(1, r.sbVal, sbBitCt, r.hole->linkedHole);
			setModelMatrix(modelMatrixBackup);
		}
	}
	if (debugPrint)uRoom::debugDepth--;
	uRoom::debugPrint = false;
}


void uRoom::renderSub(int renderTreeLevel, int SBval, int sbBitsUsed, std::shared_ptr<uHole> superHole) {
	if (!visible) return;
	if (debugPrint) {
		for (int i = 0; i < debugDepth; i++) {
			std::cout << " | ";
		}
		std::cout << name << "\n";
		uRoom::debugDepth++;
	}
	std::vector<RoomInstance> roomStack;
	extern TestCamera eyeCam;

	RoomInstance superRoom = RoomInstance(superHole);

	//find relevant holes
	for (int i = 0; i < holeChildren.size(); i++) {
		auto hole = holeChildren[i];
		if (hole == superRoom.hole->linkedHole)	continue;

		auto holeMesh = Fizz::makePolyMesh(hole->meshes[0]); //TODO: Pregen these PolyMeshes
		if (Fizz::viewContains(eyeCam.getViewProjMatrix(), holeMesh)) {
			roomStack.push_back(RoomInstance(holeMesh, hole));
		}
	}

	int sbBitCt = std::ceil(std::log2(roomStack.size() + 1));

	// Process child geom
	for (int i = 0; i < modelChildren.size(); i++) {
		auto model = modelChildren[i];
		bool isTrans = false;

		if (model->isStatic) {
			if (Fizz::polyIntersection(superRoom.mesh, model->simpleHull)) {
				superRoom.addTrans(model);
				isTrans = true;
			}
			for (RoomInstance holeset : roomStack) {
				//Optimization? : Check frustrum first
				if (Fizz::polyIntersection(holeset.mesh, model->simpleHull)) {
					holeset.addTrans(model);
					isTrans = true;
				}
			}
		}
		if (!isTrans) {
			//glDisable(GL_STENCIL_TEST);
			glEnable(GL_STENCIL_TEST);
			glStencilFunc(GL_EQUAL, SBval, 0xFF);
			model->render();
		}
	}

	//Process holes
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	int possibleRoomCt = roomStack.size();
	if (sbBitCt + sbBitsUsed > 8)std::min(possibleRoomCt, int(pow(2, sbBitCt) - 1));
	//std::cout << "(Sub)Holing for " << name << "\n";
	for (int i = 0; i < possibleRoomCt; i++) {

		//add 1 because 0x00 is no hole
		roomStack[i].sbVal = SBval + ((i + 1) << (8 - (sbBitsUsed + sbBitCt)));

		//Safety(idiot) check: the bits used by super and current should be exclusive, so + and OR should be equivalent
		//assert(roomStack[i].sbVal == SBval | (i + 1) << (8 - (sbBitsUsed + sbBitCt)));
		//Verify SBval
		assert(roomStack[i].sbVal < 0xFF);
		//std::cout << "SBval=" << std::bitset<8>(roomStack[i].sbVal) << std::endl;

		glStencilFunc(GL_EQUAL, roomStack[i].sbVal, bitMask8(0, sbBitsUsed + 1)); //Only test for parent's bits
		glStencilMask(0xFF); // Write to stencil buffer //TODO: glStencilMask(bitMask8(sbBitsUsed,8)) should be equivalent
		glDepthMask(GL_TRUE); // Write to depth buffer //Unnecissary?
							   //draw to SB
		roomStack[i].hole->render();
	}
	for (int i = 0; i < roomStack.size(); i++) {
		//clear the depthBuffer for holes
		glStencilFunc(GL_EQUAL, roomStack[i].sbVal, 0xFF);
		glStencilMask(0x00); //Shouldn't be neccesary
		glDisable(GL_DEPTH_TEST); //Depth testing was taken care of when the SB was drawn to
		//Draw to depth buffer, setting to all the way deep
		// (?'-')????.*???
		//Adam's Magic Here
		glEnable(GL_DEPTH_TEST);

		//Trans geometry
		clipPlane(0, roomStack[i].hole->clipPlanePos(), roomStack[i].hole->clipPlaneNormal());
		glDepthMask(GL_TRUE);//Unnecissary?
		glStencilMask(0x00);
		glDisable(GL_STENCIL_TEST);

		//Render on this side
		for (auto geom : roomStack[i].transGeom) {
			geom->render();
		}

		assert("adam is this the right way to invert the clip plane? -B" && 1);
		clipPlane(0, roomStack[i].hole->clipPlanePos(), -roomStack[i].hole->clipPlaneNormal());
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_EQUAL, roomStack[i].sbVal, 0xFF); //Test for SBval in SB
		glStencilMask(0x00); //Don't write to SB

		//Render on other side
		for (auto geom : roomStack[i].transGeom) {
			geom->render();
		}

		if (debugPrint) {
			for (int i = 0; i < debugDepth; i++) {
				std::cout << " | ";
			}
			std::cout << "[" << roomStack[i].room->name << "]\n";
		}
		clipPlaneOff(0);
	}

	//process Subrooms
	if (renderTreeLevel < maxDepth&&!(sbBitCt + sbBitsUsed > 8)) {
		for (int i = 0; i < possibleRoomCt; i++) {
			auto r = roomStack[i];
			auto modelMatrixBackup = getModelMatrix();
			setModelMatrix(getModelMatrix()*r.localSpaceMatrix);
			r.room->renderSub(renderTreeLevel + 1, r.sbVal, sbBitCt + sbBitsUsed, r.hole->linkedHole);
			//TODO: Finish this
			setModelMatrix(modelMatrixBackup);
		}
	}
	if(debugPrint)uRoom::debugDepth--;
}

/*void uRoom::render(std::string renderedBy){
	render(renderedBy, 0);
}
void uRoom::render(std::string renderedBy, int stencilVal){
	roomDepth++;
	//td::cout << roomDepth << "<-Roomdepth\n";
	//check to make sure roomDepth<4
	//stack[roomDepth].clear();

	//Filter for stencilbuff
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_EQUAL, stencilVal, 0xFF);
	glStencilMask(0x00);

	for (auto c : modelChildren){
		c->render(name);
	}
	for (auto c : fizzChildren){
		c->render(name);
	}
	for (auto c : lightChildren){
		c->render(name);
	}
	for (auto c : holeChildren){
		c->render(name);
	}

	//OK, here we now have the stack, so it's time to render it
	
	if (roomDepth < uRoom::maxDepth){
		for (auto& subRoom : stack[roomDepth]){
			//do render shit here
			//ADAMS PLACE
			glm::mat4x4 backup = getModelMatrix();

			setModelMatrix(subRoom.matrix);
			applyTransformations();
			subRoom.linkedRoom->render(name,subRoom.stencilValue);

			setModelMatrix(backup);
		}
	}
	roomDepth--;
}


void uRoom::render(){
	//TODO rendering sequence
}


uLink::uLink(glm::mat4x4 roomMatrix, int roomStencilValue, std::shared_ptr<uRoom> room){
	this->matrix = roomMatrix;
	this->stencilValue = roomStencilValue;
	this->linkedRoom = room;
	//std::cout << "adding linkedroom to stack: " <<room->name<<std::endl;
}*/