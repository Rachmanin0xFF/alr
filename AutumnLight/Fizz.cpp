#include "Fizz.h"

/*Test if a PolyMesh is visible within a viewProjectionMatrix*/
bool Fizz::viewContains(glm::mat4 matrix, Fizz::PolyMesh &mesh) { return true; }
/*Test if a Polymesh intersects a Hull*/
bool Fizz::polyIntersection(Fizz::PolyMesh polyMesh, Fizz::Hull hull) { return false; };
Fizz::PolyMesh Fizz::makePolyMesh(std::shared_ptr<uMesh> mesh) { return Fizz::PolyMesh(); };
