#ifndef MISCUTIL_H
#define MISCUTIL_H

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>


/* returns a string with the current date and 24h time. */
std::string dateTime24();
/* gets the parent directory of a file or folder*/
std::string getDir(std::string f);
std::vector<std::string> &sSplit(const std::string &s, char delim, std::vector<std::string> &elems);
/*splits a string over a delimiter, and returns a vector<std::string>*/
std::vector<std::string> sSplit(const std::string &s, char delim);
/*splits a string over a delimiter, and returns a vector<float>*/
std::vector<float> sSplitToFloat(const std::string &s, char delim);

/*Returns an int with @param start through @param-1 end of the least significant eight bits 1s
(first bit has index 0)*/
int bitMask8(unsigned int start, unsigned int end);
int randInt();
#endif