// ALM Importer/Parser
#include <vector>
#include "uModel.h"
#include "uHole.h"
#include "uRoom.h"
#include "uMesh.h"
#include "uMaterial.h"
#include "almLoader.h"
#include "GLUtil.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include "miscUtil.h"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/quaternion.hpp>

#define GLM_SWIZZLE
#include <glm/glm.hpp>

#include "Tesseract.h"

#include "uRef.h"
#include "uLight.h"
#include "uFizz.h"

void ALM::splitOnFirstSpace(std::string in, std::string& kwd, std::string& data){
	auto first_space = in.find(' ');
	
	if (first_space == std::string::npos){
		kwd = in;
		data = "";
	}
	else{
		kwd = in.substr(0, first_space);
		data = in.substr(first_space + 1);
	}
}

//std::map < std::string, int> genAliasMap(std::vector<std::shared_ptr<uObject>> vec){
//	std::map<std::string, int> aliases;
//	for (int i = 0; i < vec.size(); i++){
//		aliases[vec[i]->name] = i;
//	}
//	return aliases;
//}
/**
Loads a specified ALM file into the provided vector<uRoom>
@param roomList: The vector<uRoom> to load rooms into
@param location: the location of the .alm file
*/
void ALM::loadALMFile(std::vector<std::shared_ptr<uRoom>> &roomList, std::string location){

	std::clog << "(loadALMFile()) Loading .ALM multi-object (model) file from: " << location << std::endl;
	std::cout << "(loadALMFile()) Loading .ALM multi-object (model) file from: " << location << std::endl;
	std::ifstream alm_file(location.c_str());

	if (!alm_file.is_open()) {
		std::cerr << "Could not open .alm file " << location << std::endl;
		return;
	}


	//---------Prepare------------//
	//these are for: 
	std::vector<std::shared_ptr<uModel>> vModels;
	std::vector<std::shared_ptr<uFizz>> vFizzs;
	std::vector<std::shared_ptr<uLight>> vLights;
	std::vector<std::shared_ptr<uHole>> vHoles;
	std::vector<std::shared_ptr<uRef>> vRefs;
	std::vector<std::shared_ptr<uRoom>> vRooms;

	// Stores rooms and names
	std::map < std::string, std::shared_ptr<uRoom>> roomMap;

	// Load current rooms into roomMap
	for (auto& room : roomList){
		roomMap[room->name] = room;
	}

	std::map<std::string, std::shared_ptr<uMaterial>> materials;

	std::shared_ptr<uObject> ob; //Reference to the current object as a uObject, used for setting common attributes.

	std::shared_ptr<uObject> dumpy = std::make_shared<uModel>(); // TODO handle more elegantly //A place to default to for unused objects 

	enum class Type{ MODEL, FIZZ, LIGHT, HOLE, REF, ROOM, CAMERA };

	Type type;
	int index;
	// Keep track of the type of the current object, and its index in its respective vector
	std::string name;

	std::string raw_line;
	while (std::getline(alm_file, raw_line)){
		//Process line
		std::string kwd;
		//Line predicate
		std::string data;
		splitOnFirstSpace(raw_line, kwd, data);
		std::stringstream dat{ data };
		//-------------------Do the Line Thing Here--------------------------//
		//-------------------------------//Object
		if (kwd == "object"){
			//set the name
			name = data;
		}
		else
		//-------------------------------//endObject
		if (kwd == "endObject"){
			//Nothing, for now
		}
		else
		//-------------------------------//Type
		if (kwd == "type"){
			std::string typeName;
			//type data
			std::string info;
			splitOnFirstSpace(data, typeName, info);
			if (typeName == "MESH"){
				type = Type::MODEL;
				vModels.push_back(std::make_shared<uModel>(name));
				index = vModels.size() - 1;
				ob = vModels[index];
				vModels[index]->addMesh(std::make_shared<uMesh>(name));
			}
			else if (typeName == "HOLE"){
				type = Type::HOLE;
				vHoles.push_back(std::make_shared<uHole>(name));
				index = vHoles.size() - 1;
				ob = vHoles[index];
				vHoles[index]->addMesh(std::make_shared<uMesh>(name));
			}
			else if (typeName == "ROOM"){
				type = Type::ROOM;
				vRooms.push_back(std::make_shared<uRoom>(name));
				index = vRooms.size() - 1;
				auto checkExists=roomMap.find(info);
				if (checkExists == roomMap.end()){
					roomMap[info] = vRooms[index];
				}
				else {
					std::cerr << "Duplicate room with key \"" << index << "\"!\n";
				}
				ob = vRooms[index];
			}
			else if (typeName == "ROOMREF"){
				type = Type::REF;
				vRefs.push_back(std::make_shared<uRef>(name));
				index = vRefs.size() - 1;
				vRefs[index]->roomFlag=info;
				ob = vRefs[index];
			}
			else if (typeName == "LAMP"){
				type = Type::LIGHT;
				vLights.push_back(std::make_shared<uLight>(name));
				index = vLights.size() - 1;
				ob = vLights[index];
			}
			else if (typeName == "CAMERA"){
				type = Type::CAMERA;
				ob = dumpy;
			}
			else {
				std::cerr << "(loadALMFile) ERROR: unidentified type: " <<
					typeName << std::endl; ob = dumpy;
			}
		}
		else
		//-------------------------------//pos
		if (kwd == "loc"){
			glm::vec3 vec;
			dat >> vec.x >> vec.y >> vec.z;
			ob->pos = vec;
		}
		else
			//-------------------------------//rot
		if (kwd == "rot"){
			glm::quat vec;
			dat >> vec.w >> vec.x >> vec.y >> vec.z;
			ob->rot = vec;
		}
		else
					//-------------------------------//scale
		if (kwd == "scale"){
			glm::vec3 vec;
			dat >> vec.x >> vec.y >> vec.z;
			ob->scale = vec;
			//std::cout << ob->scale.x<<std::endl;
		}
		else
		//-------------------------------//q
		if (kwd == "q"){
			if (type == Type::MODEL || type==Type::HOLE){
				glm::vec3 pos;
				glm::vec2 uv;
				glm::vec3 norm;
				//////////////////////////////////
				/* WARNING: This method of loading numbas is fast, but will take ANYTHING as input. Text? Sure! Reeeely big numbas? Sure!
				Not enough values? You betcha!!! (Be careful, if something goes wrong, check here)
				*////////////////////////////////
				dat >> pos.x >> pos.y >> pos.z >> uv.x >> uv.y >> norm.x >> norm.y >> norm.z;
				//Uncomment this if textures look flipped (OpenGL likes its texture data put in y-inverted).
				//uv.y = 1.0 - uv.y;
				if (type == Type::HOLE){
					vHoles[index]->meshes[0]->addSuperPoint(pos, uv, norm);
				}
				else{
					vModels[index]->meshes[0]->addSuperPoint(pos, uv, norm);
				}
				
			}
			else {
				std::cerr << "(loadALMFile): ERROR: superpoint found in non-model object. listed name: " <<
					name << std::endl;
			}
		}
		else
		//-------------------------------//f
		if (kwd == "f"){
			if (type == Type::MODEL || type == Type::HOLE){
				int a;
				int b;
				int c;
				char ch1;
				char ch2;
				//////////////////////////////////
				/* WARNING: This method of loading numbas is fast, but will take ANYTHING as input. Text? Sure! Reeeely big numbas? Sure!
				Not enough values? You betcha!!! (Be careful, if something goes wrong, check here)
				*/////////////////////////////////
				dat >> a >> ch1 >> b >> ch2 >> c;

				if (type == Type::HOLE){
					1;
					vHoles[index]->meshes[0]->addFace(--a, --b, --c);
				}else{
					vModels[index]->meshes[0]->addFace(--a, --b, --c);
				}
			}
			else {
				std::cerr << "(loadALMFile): ERROR: face indices found in non-model object. listed name: " <<
					name << std::endl;
			}
		}
		else
		//-------------------------------//Parent
		if (kwd == "parent"){
			ob->parentFlag = data;
		}
		else
		//-------------------------------//usemtl
		if (kwd == "usemtl"){
			if (type == Type::MODEL){
				vModels[index]->meshes[0]->materialFlag = data;
			}
			else std::cerr << "(loadALMFile): ERROR #1.1\n";
		}
		else
		//-------------------------------//useGeom
		if (kwd == "useGeom"){
			if (type == Type::MODEL){
				vModels[index]->F_usegeom = data;
			}
			else if (type == Type::HOLE){
				vHoles[index]->F_usegeom = data;
			}
			else std::cerr << "(loadALMFile): ERROR #1.2: useGeom set on non model or hole\n";
		}
		else
		//-------------------------------//s
		if (kwd == "s"){
			if (type == Type::MODEL){
				if (data == "off" || data == "0") vModels[index]->meshes[0]->smoothShading = false;
				else if (data == "on" || data == "1") vModels[index]->meshes[0]->smoothShading = true;
				else std::cerr << "(loadALMFile) ERROR: unidentified smoothing value: " << data << std::endl;
			}
			else std::cerr << "(loadALMFile): ERROR #2\n";
		}
		else if (kwd == "mtllib"){
			materials = ALM::loadMTL(location.substr(0, location.length() - 4) + ".mtl");
		}
	}
	//ok, now we have lists of each of the types of objects.
	//woi lets do stuff!
	std::cout << "#### ROOMS ####\n";
	for (auto iterator = roomMap.begin(); iterator != roomMap.end();iterator++){
		std::cout << iterator->first<<std::endl;
	}

	//stores object type and index within that type's vector
	std::map<std::string, std::tuple<Type, int>> aliases;
	for (int i = 0; i < vModels.size(); i++){
		aliases[vModels[i]->name] = std::tuple<Type, int>(Type::MODEL, i);
	}
	for (int i = 0; i < vLights.size(); i++){
		aliases[vLights[i]->name] = std::tuple<Type, int>(Type::LIGHT, i);
	}
	for (int i = 0; i < vRefs.size(); i++){
		aliases[vRefs[i]->name] = std::tuple<Type, int>(Type::REF, i);
	}
	for (int i = 0; i < vFizzs.size(); i++){
		aliases[vFizzs[i]->name] = std::tuple<Type, int>(Type::FIZZ, i);
	}
	for (int i = 0; i < vRooms.size(); i++){
		aliases[vRooms[i]->name] = std::tuple<Type, int>(Type::ROOM, i);
	}
	for (int i = 0; i < vHoles.size(); i++){
		aliases[vHoles[i]->name] = std::tuple<Type, int>(Type::HOLE, i);
	}

	//-------------------//uLights
	for (auto& thing : vLights){
		if (thing->parentFlag == "")  continue;
		auto parent = aliases.find(thing->parentFlag);
		if (parent != aliases.end()){
			Type type = std::get<0>(parent->second);
			int index = std::get<1>(parent->second);
			switch (type){
			case Type::MODEL:
				vModels[index]->addChild(thing);
				break;
			case Type::FIZZ:
				vFizzs[index]->addChild(thing);
				break;
			case Type::LIGHT:
				vLights[index]->addChild(thing);
				break;
			case Type::REF:
				vRefs[index]->addChild(thing);
				break;
			case Type::ROOM:
				vRooms[index]->addChild(thing);
				break;
			default:
				std::cerr << "(loadALMFile) ERROR 4: unsupported parent: " << (int)type << std::endl;
			}
		}
		else{
			std::cerr << "(loadALMFile) ERROR 3: parent alias not found : " << thing->parentFlag << " " << thing->name << std::endl;
		}
	}

	//-------------------//uRefs
	for (auto& thing : vRefs){
		if (thing->parentFlag == "")  continue;
		auto parent = aliases.find(thing->parentFlag);
		if (parent != aliases.end()){
			Type type = std::get<0>(parent->second);
			int index = std::get<1>(parent->second);
			if (type == Type::HOLE){
				//make child of hole
				std::cerr << "MAKING CHILD\n";
				assert(!(vHoles[index]->ref)&&"a hole should only have one uRef. If it already has one, we have a problem.");
				vHoles[index]->ref=thing;
				//throw 2;
			}
			else {
				std::cerr << "NOT MAKING CHILD\n";
				//throw 3;
			}
			/*switch (type){
			case Type::MODEL:
				vModels[index]->addChild(thing);
				break;
			case Type::FIZZ:
				vFizzs[index]->addChild(thing);
				break;
			case Type::LIGHT:
				vLights[index]->addChild(thing);
				break;
			case Type::REF:
				vRefs[index]->addChild(thing);
				break;
			case Type::ROOM:
				vRooms[index]->addChild(thing);
				break;
			default:
				std::cerr << "(loadALMFile) ERROR 4: unsupported parent: " << (int)type << std::endl;
			}*/
		}
		else{
			std::cerr << "(loadALMFile) ERROR 3: parent alias not found : " << thing->parentFlag << " " << thing->name << std::endl;
		}
		if (thing->parentFlag == "")  std::cout << "AAAHHH empty room for uref " << thing->name << std::endl;
		auto referal = roomMap.find(thing->roomFlag);
		if (referal != roomMap.end()){
			thing->linkedRoom = (referal->second);
		}
		else std::cout << "AAAHHH unknown room key "<<thing->roomFlag<<" for uref " << thing->name << std::endl;
	}
	//-------------------//uModels
	for (auto& thing : vModels){
		if (thing->parentFlag != ""){
			auto parent = aliases.find(thing->parentFlag);
			if (parent != aliases.end()){
				Type type = std::get<0>(parent->second);
				int index = std::get<1>(parent->second);
				switch (type){
				case Type::MODEL:
					vModels[index]->addChild(thing);
					break;
				case Type::FIZZ:
					vFizzs[index]->addChild(thing);
					break;
				case Type::LIGHT:
					vLights[index]->addChild(thing);
					break;
				case Type::REF:
					vRefs[index]->addChild(thing);
					break;
				case Type::ROOM:
					vRooms[index]->addChild(thing);
					break;
				default:
					std::cerr << "(loadALMFile) ERROR 4: unsupported parent: " << (int)type << std::endl;
				}
			}
		}
		else{
			uniModels.push_back(thing);
			std::cerr << "(loadALMFile) ERROR 3: parent alias not found : " << thing->parentFlag << " " << thing->name << std::endl;
		}
		if (thing->F_usegeom != ""){
			auto super = aliases.find(thing->F_usegeom);
			if (super != aliases.end()){
				Type type = std::get<0>(super->second);
				int index = std::get<1>(super->second);
				
				if (type == Type::MODEL){
					// Add mesh from model
					thing->meshes[0] = vModels[index]->meshes[0];
					//std::cout << thing->name << " getting geom from " << vModels[index]->name << std::endl;
				}
				else if (type == Type::FIZZ){
					// Add mesh from model
					thing->meshes[0] = vModels[index]->meshes[0];
					//std::cout << thing->name << " getting geom from " << vFizzs[index]->name << std::endl;
				}
				else {
					1;
					std::cerr << "(loadALMFile) ERROR 5. (Debug: " << thing->F_usegeom << ")\n";
				}
			}
			else {
				std::cerr << "Unable to find referenced geometry: " << thing->F_usegeom << "\n";
			}
		}
		else {
			for (auto m : thing->meshes) {
				std::cerr << "Building mesh for [" << thing->name << "](Parent geom flag [" << thing->F_usegeom << "]\n";
				m->build();
				auto mat = materials.find(m->materialFlag);
				if (mat != materials.end()) {
					m->material = mat->second;
				}
				else std::cerr << "(lALMF) ERROR 6 no material.";
			}
		}
	}
	//-------------------//uHoles
	for (auto& thing : vHoles){
		if (thing->parentFlag != ""){
			auto parent = aliases.find(thing->parentFlag);
			if (parent != aliases.end()){
				Type type = std::get<0>(parent->second);
				int index = std::get<1>(parent->second);
				switch (type){
				case Type::MODEL:
					vModels[index]->addChild(thing);
					break;
				case Type::FIZZ:
					vFizzs[index]->addChild(thing);
					break;
				case Type::LIGHT:
					vLights[index]->addChild(thing);
					break;
				case Type::REF:
					vRefs[index]->addChild(thing);
					break;
				case Type::ROOM:
					vRooms[index]->addChild(thing);
					break;
				default:
					std::cerr << "(loadALMFile) ERROR 4: unsupported parent: " << (int)type << std::endl;
				}
			}
		}
		else{
			uniModels.push_back(thing);
			std::cerr << "(loadALMFile) ERROR 3: parent alias not found : " << thing->parentFlag << " " << thing->name << std::endl;
		}
		if (thing->F_usegeom != ""){
			auto super = aliases.find(thing->F_usegeom);
			if (super != aliases.end()){
				Type type = std::get<0>(super->second);
				int index = std::get<1>(super->second);

				if (type == Type::MODEL){
					// Add mesh from model
					thing->addMeshes(vModels[index]->meshes);
					//std::cout << thing->name << " getting geom from " << vModels[index]->name << std::endl;
				}
				else if (type == Type::FIZZ){
					// Add mesh from model
					thing->addMeshes(vFizzs[index]->meshes);
					//std::cout << thing->name << " getting geom from " << vFizzs[index]->name << std::endl;
				}
				else {
					1;
					std::cerr << "(loadALMFile) ERROR 5. (Debug: " << thing->F_usegeom << ")\n";
				}
			}
		}
		for (auto m : thing->meshes){
			std::cerr << "Building mesh for [" << thing->name << "](Parent geom flag ["<<thing->F_usegeom<<"]\n";
			m->build();
			auto mat = materials.find(m->materialFlag);
			if (mat != materials.end()){
				m->material = mat->second;
			}
			else std::cerr << "(lALMF) ERROR 6 no material.";
		}
	}
	for (auto r : vRooms){
		roomList.push_back(r);
	}
	ALM::linkHoles(roomList);
}/*
	//prepare alias list
	std::map<std::string, int> roomAliases;
	for (int i = 0; i < roomList.size(); i++){
		roomAliases[roomList[i].name] = i;
	}

	//Create list to put new objects into.
	flaggedObject* cur=new flaggedObject();
	std::vector<flaggedObject> flagged;
	std::shared_ptr<uMesh> mesh(new uMesh());
	std::string raw_line;

	// Main read loop
	while (std::getline(alm_file, raw_line)){
		//Process line
		auto first_space = raw_line.find(' ');
		std::string kwd;
		std::string data;
		if (first_space == std::string::npos){
			kwd = raw_line;
			data = "";
		}
		else{
			kwd = raw_line.substr(0, first_space);
			data = raw_line.substr(first_space + 1);
		}
		std::stringstream dat{ data };

		std::cout << "kwd: " << kwd << "\n";

		//---------------------------// Object
		if (kwd == "object"){
			cur = new flaggedObject();
			cur->name = data;
		}else
		//---------------------------// endObject
		if (kwd == "endObject"){
			std::cout << "here we are.\n";
			if (cur->type == "MESH"){
				cur->ob->
			}
			flagged.push_back(*cur);
		}else
		//---------------------------// Type
		if (kwd == "type"){
			std::string typeS;
			std::string info;
			auto second_space = data.find(' ');
			if (second_space == std::string::npos){
				typeS=data;
				info = "";
			}
			else{
				typeS = data.substr(0, second_space);
				info = raw_line.substr(first_space);
			}
			if (typeS == "ROOM"){
				cur->ob = std::make_shared<uRoom>(info);
			}else if (typeS == "ROOMREF"){
				cur->ob = std::make_shared<uRoomRef>(info);
			}
			else if (typeS == "MESH"){
				cur->ob = std::make_shared<uModel>();
				mesh = std::make_shared<uMesh>();
				cur->type = "MESH";
			}
			else std::cerr << "Error in loadALMFile: unidentified object type \"" << data << "\"\n";
		}else
		//---------------------------// superPoints
		if (kwd == "q"){
			glm::vec3 pos;
			glm::vec2 uv;
			glm::vec3 norm;
			//////////////////////////////////
			/* WARNING: This method of loading numbas is fast, but will take ANYTHING as input. Text? Sure! Reeeely big numbas? Sure! 
			Not enough values? You betcha!!! (Be careful, if something goes wrong, check here)
			////////////////////////////////
			dat >> pos.x >> pos.y >> pos.z >> uv.x >> uv.y >> norm.x >> norm.y >>
				norm.z;
            // Do mesh stuff
			mesh->addSuperPoint(pos, uv, norm);
			/*std::cout << "Q: " <<
				pos.x << " " << pos.y << " " << pos.z << " "<<
				uv.x<<" "<<uv.y<< " "<<
				norm.x << " " << norm.y << " "<< norm.z
				<< "\n";
				/
		}
		if (kwd == "f"){
			int a;
			int b;
			int c;
			char ch1;
			char ch2;
			//////////////////////////////////
			/* WARNING: This method of loading numbas is fast, but will take ANYTHING as input. Text? Sure! Reeeely big numbas? Sure!
			Not enough values? You betcha!!! (Be careful, if something goes wrong, check here)
			/////////////////////////////////
			dat >> a >> ch1 >> b >> ch2 >> c;
			//std::cout << "chrs: " << ch1 << " " << ch2 << std::endl;
			// Do mesh stuff
			mesh->addFace(a, b, c);
		}

	}

	//Process the flaggedObjects
	std::map<std::string, int> aliases;
	for (int i = 0; i < flagged.size(); i++){
		aliases[flagged[i].name] = i;
	}
	for (auto& flgd : flagged){
		if (flgd.Fparent != ""){
			flagged[aliases[flgd.Fparent]].ob->addChild(flgd.ob);
		}
		if (flgd.type == "MESH"){
			uModel& mf(dynamic_cast<uModel&>(*flgd.ob));
			if (flgd.Fgeom != ""){
				uModel& mf2(dynamic_cast<uModel&>(*flagged[aliases[flgd.Fgeom]].ob));
				mf.addMeshes(mf2.meshes);
			}
			flgd.ob = mf;
		}/
	}
	std::cout << "DEBUG 1: " << "aliases size:";
}
/** loads .alm file and returns a vector<uModel*> of pointers to top-level objects. 
usage: loadALM(std::vector<uModel*> &parentPointers, std::string location) /
void loadALM(std::vector<std::shared_ptr<uModel>> &parentPointers, std::string location){
	//Notes: these notes are not right.s
	/*
	modlib is the home of all the models under uRoom

	matlib is used to store all the materials from the mtl. Since we use pointers to 
	refer to materials from uMesh, those pointers need to point to uMaterials somewhere.
	uMeshes are stored with unique copies in each uModel, although in the future we 
	might want to have a meshlib, so that we don't have 30 copies of the same barrel mesh.
	We could also consider splitting up uMaterials to go in meshes, which is less efficient,
	but means we could do things like load an alm under a uModel. This could also be
	accomplished by searching up the tree and finding the top-level mesh/model storage.
	/
	std::clog << "Loading .ALM multi-object (model) file from: " << location << std::endl;
	std::cout << "Loading .ALM multi-object (model) file from: " << location << std::endl;
	std::ifstream alm_file(location.c_str());

	if (!alm_file.is_open()) {
		std::cerr << "Could not open .alm file " << location << std::endl;
		return;
	}

	std::vector<std::shared_ptr<uModel>> retPointers;
	std::map<std::string, uMaterial> materials; //pointers to matlib. used for lookup by name
	std::map<std::string, flaggedModel> models; //generated model pointers and flags.
	//Initial variable creation
	uMesh cmesh; //temporary storage, MUST be copied to some place so pointers work.
	uModel cmodel; //temporary storage, MUST be copied to some place so pointers work.
	bool firstModel = true;
	std::string modelName;
	std::string flag_parent = "";
	std::string flag_geom = "";
	std::string flag_mtl = "";
	std::string flag_smooth = "";
	
	std::string raw_line;
	std::vector<float> tv;


	while (std::getline(alm_file, raw_line)){
		const char *cline = raw_line.c_str();
		//std::cout << "HIEEEE" << sizeof(line) << "\n";
		//strcpy_s(cline, sizeof(line),line.c_str());
		//std::cout <<"cline: "<< cline <<"| "<<strncmp(cline,"object ",7)<< std::endl;
		if (cline && cline[0] == '\0'){
			//std::cout << "skipping" << std::endl;
			continue; //If line is nothing or a comment, skip ahead
		}
		//std::cerr << std::string.find_first_of(' ') << std::endl;
		std::string line;
		if (raw_line.find_first_of(' ')!=std::string::npos){
			line = raw_line.substr(raw_line.find_first_of(' ')+1); //MAKE FASTER FUNCTION
		}
		else{
			line = raw_line;
		}
		//std::cout << "LINE: " << line<<"||| Cstr: "<<cline<<std::endl;
		// do faces and poinst first because they are more likely
		if (strncmp(cline, "q ", 2)==0){
			//std::cout << "QUE";
			//do superpoint
			tv = sSplitToFloat(line, ' ');
			// order: geom, uv, normal
			cmesh.addSuperPoint(
				glm::vec3(tv[0], tv[1], tv[2]),
				glm::vec2(tv[3], tv[4]),
				glm::vec3(tv[5], tv[6], tv[7])
				);

		}
		else if (strncmp(cline, "f ", 2) == 0){
			//do face
			tv = sSplitToFloat(line, '/');
			// geom, uv, normal
			cmesh.addFace(tv[0]-1, tv[1]-1, tv[2]-1);
		}
		else if (strncmp(cline, "object ", 7) == 0){
			//start new object
			std::cout << "OBJECTING WOO!!!!\n";
			if (!firstModel){
				uModel *modelPtr = new uModel;
				*modelPtr = uModel(cmodel);
				cmesh.initialVerts = cmesh.stream[0].size();
				cmesh.initialFaces = cmesh.faces.size();
				cmesh.build();
				if (flag_geom == ""){
					//only add the mesh when we won't get it from somewhere else
					modelPtr->addMesh(cmesh);
				}
				modelPtr->name = modelName;
				models[modelName] = flaggedModel(modelPtr, flag_parent, flag_geom, flag_mtl, flag_smooth);
				std::cout << "\tNAME: " << modelName <<" numba: "<< models.size();
			}
			modelName = sSplit(line, ' ')[0];
			firstModel = false;
			cmodel = uModel(modelName);
			cmesh = uMesh();
			flag_geom = "";
			flag_mtl = "";
			flag_parent = "";
			flag_smooth = "";
		}
		else if (strncmp(cline, "parent ", 7) == 0){
			//do superpoint
			flag_parent = sSplit(line, ' ')[0];
		}
		else if (strncmp(cline, "loc ", 4) == 0){
			//do location
			tv = sSplitToFloat(line, ' ');
			cmodel.pos = glm::vec3(tv[0], tv[1], tv[2]);
		}
		else if (strncmp(cline, "rot ", 4) == 0){
			//do superpoint
			tv = sSplitToFloat(line, ' ');
			cmodel.rot = glm::quat(tv[0], tv[1], tv[2], tv[3]);
		}
		else if (strncmp(cline, "usemtl ", 7) == 0){
			//do superpoint
			flag_mtl = sSplit(line, ' ')[0];
		}
		else if (strncmp(cline, "s ", 2) == 0){
			//check smooth shading
			flag_smooth = sSplit(line, ' ')[0];
		}
		else if (strncmp(cline, "useGeom ", 8) == 0){
			//do superpoint
			flag_geom = sSplit(line, ' ')[0];
		}
		else if (strncmp(cline, "mtllib ", 7) == 0){

			materials = loadMTL(location.substr(0,location.length()-4)+".mtl");
		}
		else std::cout << "not parsed: " << line << std::endl;
		
	}
	uModel *modelPtr = new uModel;
	*modelPtr = uModel(cmodel);
	cmesh.initialVerts = cmesh.stream[0].size();
	cmesh.initialFaces = cmesh.faces.size();
	cmesh.build();
	if (flag_geom == ""){
		//only add the mesh when we won't get it from somewhere else
		modelPtr->addMesh(cmesh);
	}
	modelPtr->name = modelName;
	models[modelName] = flaggedModel(modelPtr, flag_parent, flag_geom, flag_mtl, flag_smooth);
	std::cout << "\tNAME: " << modelName << " numba: " << models.size();

	std::cout << "Generated " << models.size() << " models.\n";
	//// Handle flags ////
	//reused vars for memory efficency
	//uModel* pParent;
	//std::cout << "TEST:" << materials["material"] == materials[]
	for (auto& p : models){
		std::shared_ptr<uModel> m;
		m.reset(p.second.model);
		flaggedModel f = p.second;
		f.printAll();
		if (f.parent == ""){
			retPointers.push_back(m);
		}
		else {
			//parent_iter = models.find(flag_parent);
			try {

				models.at(f.parent).model->addChild(m);
			}
			catch (const std::out_of_range& oor) {
				std::cerr << "Out of Range error: " << oor.what() << " Could not parent object " << p.first << '\n';
			}
		}
		if (f.geom != ""){
			try {
				m->addMeshes(models.at(f.geom).model->meshes);
			}
			catch (const std::out_of_range& oor) {
				std::cerr << "Out of Range error: " << oor.what() << " Could not find geometry source for object " << p.first << '\n';
			}
		}
		if (f.mtl != ""){
			m->meshes[0]->material = materials[f.mtl];
			std::cout << "setting material\n";
		}
		if (f.smooth == "on"){
			m->meshes[0]->material.do_smooth_shading = true;
			//std::cout << "setting material\n";
		}
		if (f.smooth == "off"){
			m->meshes[0]->material.do_smooth_shading = false;
			//std::cout << "setting material\n";
		}
	}
	std::cout << "Generated " << retPointers.size() << " pointers.\n";
	parentPointers = retPointers;
}
*/
std::map<std::string, std::shared_ptr<uMaterial>> ALM::loadMTL(std::string location) {
	std::cout << "Loading .MTL multi-material file from: " << location << std::endl;

	std::ifstream mtl_file(location.c_str());
	std::string line;
	if (!mtl_file.is_open()) {
		std::cout << "Could not open .MTL file " << location << std::endl;
		std::cerr << "Could not open .MTL file " << location << std::endl;
		//return;
	}

	std::map<std::string, std::shared_ptr<uMaterial>> materials;
	bool first_mtl = true;
	std::shared_ptr<uMaterial> current_material=std::make_shared<uMaterial>();
	while (std::getline(mtl_file, line)) {
		if (line.find("newmtl") == 0) {
			materials[current_material->name] = current_material;
			current_material=std::make_shared<uMaterial>();
			current_material->name = line.substr(7);
			std::cout << "Loading material with name: " << current_material->name << '\n';
		}

		if (line.find("Ns") == 0) {
			current_material->hardness = ((std::stof(line.substr(3)) / 1.96078431373) + 1.f);
		}
		if (line.find("Ka") == 0) {
			std::vector<std::string> d = sSplit(line, ' ');
			float r = std::stof(d[1]);
			float g = std::stof(d[2]);
			float b = std::stof(d[3]);
			current_material->ambient_intensity = (r + g + b) / 3.f;
		}
		//NOTE: The code in the following lines is used to normalize the light color, storing it more sensibly.
		//If a light's color is dark brown, it will set the color to orange and turn down the light brightness.
		//The new dim orange light is identical to the "brown" one and its properties make more sense.
		if (line.find("Kd") == 0) {
			std::vector<std::string> d = sSplit(line, ' ');
			float r = std::stof(d[1]);
			float g = std::stof(d[2]);
			float b = std::stof(d[3]);
			float div = fmax(fmax(r, g), b);
			float r2 = r / div;
			float g2 = g / div;
			float b2 = b / div;
			current_material->diffuse_color = glm::vec3(r2, g2, b2);
			current_material->diffuse_intensity = div;
		}
		if (line.find("Ks") == 0) {
			std::vector<std::string> d = sSplit(line, ' ');
			float r = std::stof(d[1]);
			float g = std::stof(d[2]);
			float b = std::stof(d[3]);
			float div = fmax(fmax(r, g), b);
			float r2 = r / div;
			float g2 = g / div;
			float b2 = b / div;
			current_material->specular_color = glm::vec3(r2, g2, b2);
			current_material->specular_intensity = div;
		}
		if (line.find("map_Ka") == 0) {
			std::string texName = line.substr(7).c_str();
			if (texName.find_last_of("\\/") == std::string::npos)
				texName = getDir(location) + "/" + texName;
			current_material->ambient_texture = uLoadImage(texName.c_str());
			current_material->use_ambient_texture = true;
		}
		if (line.find("map_Kd") == 0) {

			//std::vector<std::string> location_split = split(location, '/');
			//std::string floc = "";
			//for (int i = 0; i < location_split.size() - 1; i++) {
			//	floc += location_split[i] + "/";
			//}
			//floc += line.substr(7);
			//loadMaterials(floc.c_str());
			std::string texName = line.substr(7).c_str();
			if (texName.find_last_of("\\/") == std::string::npos)
				texName = getDir(location) + "/" + texName;
			current_material->albedo_texture = uLoadImage(texName.c_str());
			current_material->use_albedo_texture = true;
		}
		if (line.find("map_Bump") == 0) {
			if (line.find("C:") == 9) {
				current_material->normal_texture = uLoadImage(line.substr(9).c_str());
			}
			else {
				std::vector<std::string> location_split = sSplit(location, '/');
				std::string floc = "";
				for (int i = 0; i < location_split.size() - 1; i++) {
					floc += location_split[i] + "/";
				}
				floc += line.substr(9);
				current_material->normal_texture = uLoadImage(floc.c_str());
			}

			current_material->use_normal_texture = true;
		}
	}
	//current_material.printInfo();
	materials[current_material->name] = current_material;
	mtl_file.close();
	return materials;
}

ALM::flaggedModel::flaggedModel(){}

ALM::flaggedModel::flaggedModel(uModel* modelPtr,
		std::string flag_parent, std::string flag_geom,
		std::string flag_mtl, std::string flag_smooth){
	model = modelPtr;
	parent = flag_parent;
	geom = flag_geom;
	mtl = flag_mtl;
	smooth = flag_smooth;

}
void ALM::flaggedModel::printAll(){
	std::cout << "Model Name: " << model->name
		<< " Parent: " << parent
		<< " Geom: " << geom
		<< "\n\t Mtl: " << mtl
		<< " Smooth: " << smooth << std::endl;
}

ALM::flaggedObject::flaggedObject(){}

void ALM::linkHoles(std::vector<std::shared_ptr<uRoom>> &roomList){
	/*std::cerr << "MATS\n";
	uPrintMatrix(glm::translate(getModelMatrix(), glm::vec3(3.81,-0.78,2.05)),std::cerr);
	uPrintMatrix(glm::translate(glm::mat4(), glm::vec3(3.81, -0.78, 2.05))*getModelMatrix(), std::cerr);*/
	std::cerr << "linking holes...\n";
	for (std::shared_ptr<uRoom> room : roomList){
		std::cerr << "Room: " << room->name <<"|Number of holes: "<<room->holeChildren.size()<< std::endl;
		for (std::shared_ptr<uHole> hole : room->holeChildren){
			if (!hole->linkedHole == NULL){
				std::cerr << "prelinked\n";
				continue;
			}
			std::cerr << "  Hole: " << hole->name << std::endl;
			assert(hole->ref);
			
				auto subroom = hole->ref->linkedRoom;
				std::cerr << "    Subroom: " << subroom->name << std::endl;
				bool linked = false;
				for (std::shared_ptr<uHole> subhole : subroom->holeChildren){
					//Test for goodness
					std::cerr << "      SubHole: " << subhole->name << std::endl;
					assert(subhole->ref);
					auto ba = hole->matrix() * hole->ref->matrix();
					auto dc = subhole->matrix() * subhole->ref->matrix();
					//uPrintMatrix(ba, std::cerr);
					//uPrintMatrix(dc, std::cerr);
					//uPrintMatrix(glm::inverse(ba)*dc, std::cerr);
					if (compareMatrix(glm::inverse(ba)*dc,glm::mat4())){
						std::cerr << "    Setting hole thing\n";
						hole->linkedHole = subhole;
						subhole->linkedHole = hole;
						linked = true;
						break;
					}
				}
				if (!linked){
					std::cerr << "    Fail: No room linked" << std::endl;
				}
			
		}

	}
}