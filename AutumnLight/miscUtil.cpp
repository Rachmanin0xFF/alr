#include "miscUtil.h"
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
#include <vector>

#include<stdarg.h>

#include <tchar.h>

const static bool LOG_INFO = false;



std::string dateTime24(){
	struct tm newtime;
	__time64_t long_time;
	char timebuf[30];
	errno_t err;

	// Get time as 64-bit integer.
	_time64(&long_time);
	// Convert to local time.
	err = _localtime64_s(&newtime, &long_time);
	err = asctime_s(timebuf, 26, &newtime);
	char outbuf[30];
	sprintf_s(outbuf, "%.19s \n", timebuf);
	return std::string(outbuf);
}

std::string getDir(std::string f){
	// chops off the last chunk  of a filename to provide a parent directory
	return f.substr(0, f.find_last_of("\\/"));
}

std::vector<std::string> &sSplit(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

std::vector<std::string> sSplit(const std::string &s, char delim) {
	std::vector<std::string> elems;
	sSplit(s, delim, elems);
	return elems;
}

//std::vector<float> splitToFloat(const std::string &s, char delim){
//	std::vector<std::string> strs = split(s, delim);
//	std::vector<float> bob(strs.begin(), strs.end());
//	std::vector<float> bob;
//	return bob;
//}
std::vector<float> sSplitToFloat(const std::string &s, char delim){
	std::vector<std::string> strs = sSplit(s, delim);
	std::vector<float> outFloats;
	for (int i = 0; i < strs.size(); i++)
	{
		float num = atof(strs.at(i).c_str());
		outFloats.push_back(num);
	}
	return outFloats;
}

 //std::vector<std::string> DataNumbers;
 //   // Fill DataNumbers
 //   std::vector<int> intNumbers;
 //   for (int i=0; i<= 5; i++)
 //   {
 //    int num = atoi(DataNumbers.at(i).c_str());
 //    intNumbers.push_back(num);
 //   }

int bitMask8(unsigned int start, unsigned int end) {
	return 0xFF >> start ^ 0xFF >> end;
}


int randInt(){
	return 4;
}