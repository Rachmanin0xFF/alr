#ifndef GLOBE_H
#define GLOBE_H
/*
Hi. We are Adam Lastowka and Benjamin Welsh. This is a little file to store varibles we need 
everywhere, so we don't have to include stuff we shouldn't in places we shouldn't. Neither 
of us think this is an ideal technique, so don't take it as representing our programming. We
know better than this.
Peace, Cats in Toobs, and High framerates,
-The AL Developent team
*/

namespace Globe{
	// Stencil Buffer Tracking //----------------------------------------//
	
	//Used to track the current stencil buffer value
	//uRoom* workingRoom;
}
#endif