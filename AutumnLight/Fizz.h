#ifndef FIZZ_H
#define FIZZ_H

#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <memory>

#include "uMesh.h"

namespace Fizz{
	class PolyMesh {};
	class Hull {};

	/*Test if a PolyMesh is visible within a viewProjectionMatrix*/
	bool viewContains(glm::mat4 matrix, Fizz::PolyMesh &mesh);
	/*Test if a Polymesh intersects a Hull*/
	bool polyIntersection(Fizz::PolyMesh polyMesh, Fizz::Hull hull);
	Fizz::PolyMesh makePolyMesh(std::shared_ptr<uMesh> mesh);
}
#endif