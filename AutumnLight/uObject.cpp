#include "uObject.h"

#include "uModel.h"
#include "uHole.h"
#include "uFizz.h"
#include "uLight.h"
//#include "uRef.h"

#include <string>
#include <iostream>

void uObject::addChild(std::shared_ptr<uModel> child){
	modelChildren.push_back(child);
}
void uObject::addChild(std::shared_ptr<uFizz> child){
	fizzChildren.push_back(child);
}
void uObject::addChild(std::shared_ptr<uLight> child){
	lightChildren.push_back(child);
}
void uObject::addChild(std::shared_ptr<uHole> child){
	holeChildren.push_back(child);
}
void uObject::printChilds(int lvl){
	std::cout << name.insert(0, lvl, '|')<<" ("<<scale.x<<","<<scale.y<<","<<scale.z<<")\n";
	for (auto c : modelChildren){ c->printChilds(lvl + 1); }
	for (auto c : fizzChildren){ c->printChilds(lvl + 1); }
	for (auto c : lightChildren){ c->printChilds(lvl + 1); }
	for (auto c : holeChildren){ c->printChilds(lvl + 1); }
}
glm::mat4 uObject::matrix(){
	return glm::scale(glm::translate(glm::mat4(), pos)*glm::mat4_cast(rot),scale);// 
}