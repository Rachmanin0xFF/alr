# AutumnLight Relativity #

### What is this? ###

* It's a game engine.
* AutumnLight Relativity is being designed to handle non-euclidean environments in games. Make four right turns and end up in a different place from where you started. That sort of thing. ALR is written from the ground up in OpenGL (core profile) and C++. If you're on this page, you probably know a little bit about it already.
* Current Version: Pre-Alpha v2.0

### Features ###

* We can load ALMs!

### TODO ###

* Make a better README.md

### Contact us ###

* Send messages to adamlastowka@gmail.com for now.